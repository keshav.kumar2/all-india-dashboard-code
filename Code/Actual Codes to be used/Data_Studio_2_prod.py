#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  6 13:04:02 2022

@author: arunabhmajumdar
"""
print ("Importing all packages and Google BQ credentials files")
import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os, shutil
import json
import time
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
start_1 = time.time()
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-copy-5949defd88ea.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india-copy")
import warnings
warnings.filterwarnings("ignore")


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-production")
client = session.client("dynamodb",region_name="ap-south-1")
dynamodb = boto3.resource("dynamodb",region_name="ap-south-1")
print ("Connecting to Postgres using psycopg2")
connection = psycopg2.connect(user="keshavkumar",
                                      password="J3@!@@s..o90&2U$32-",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

print ("Function to download Postgres data and concert that to dataframe")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

print ("Function to clean dataframe to include only data post Sept 1, 2021")
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df




print ("Connecting to iam.users")
query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
print ("Cleaning phone numbers")
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2

print ("Connecting to ems.employees")
query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)

print ("Connecting to xorg.employers")
print ("This is required to get the lookup_name (company) and organization id for every user")
query = """select id, lookup_name from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)

print ("Merging the ems, iam and xorg tables")
ems_xorg = pd.merge(ems_employees, xorg, on = "employer_id")
iam_users = iam[["user_id", "full_name", "email","phone_number", "metadata","document_number"]]
iam_ems_employees = pd.merge(ems_xorg, iam_users, on = "user_id")
iam_ems_employees = iam_ems_employees.sort_values("created_at")

print ("Cleaning the metadata column of iam.users to extract birthdate and gender")
gender=[]
birth_date=[]
for i in range(0,iam_ems_employees.shape[0]):
    gender.append(iam_ems_employees["metadata"].iloc[i]["gender"])
    birth_date.append(iam_ems_employees["metadata"].iloc[i]["birth_date"])

iam_ems_employees["Gender"] = gender
iam_ems_employees["birth_date"] = birth_date
iam_ems_employees.drop(["metadata"],1,inplace=True)
iam_ems_employees = iam_ems_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',
'organization_id','document_number','created_at', "lookup_name"]]

print ("Extracting only the D2C records based on organization id")
d2c = iam_ems_employees[iam_ems_employees["organization_id"]=="916227f6-cb69-46ec-8cb1-a735ed98f2c4"]





print ("Getting the Bureau data from user.risk.verifications")
print ("This has to be combined with AWS S3 to include the reasons, Bureau reject reasons are stored in AWS ")

start = time.time()
uid = d2c["user_id"].unique().tolist()
query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)
cv.rename(columns={"score":"Bureau Approved"
                  },inplace=True)
cv = cv[cv["user_id"].isin(uid)]
cv = cv[['user_id','Bureau Approved','underwriting', 'fraud', 'kyc']]
cv = cv.fillna("API Not Hit/No Value")
cv['Bureau Approved'] = cv['Bureau Approved'].replace({False: 'Rejected', True: 'Approved'})
cv['fraud'] = cv['fraud'].replace({False: 'Rejected', True: 'Approved'})
cv['underwriting'] = cv['underwriting'].replace({False: 'Rejected', True: 'Approved'})
cv["kyc"] = cv["kyc"].replace({False: 'Rejected', True: 'Approved'})
d2c = pd.merge(d2c,cv,on = "user_id", how = "left")
d2c["Bureau Approved"] = d2c["Bureau Approved"].fillna("API Not Hit/No Value")
d2c["fraud"] = d2c["fraud"].fillna("API Not Hit/No Value")
d2c["underwriting"] = d2c["underwriting"].fillna("API Not Hit/No Value")
d2c["kyc"] = d2c["kyc"].fillna("API Not Hit/No Value")


start = time.time()
print ("Connecting to Event logs - This could take a while")
print ("This is necessary to capture every possible risk rejection reason in Fraud and underwriting and Kyc for which a D2C cusotmer can get rejected")
print ("starting event logs")
print (time.time() - start)
print ("Starting Query Run")
query = """select action, body from elog.events e;"""
elog = dataframe_generator(query)
print ("End of Query Run")
print (time.time() - start)
categories = elog["action"].value_counts().index.tolist()
print ("Categories listed")
risk = []
for x in categories:
    if x.startswith("risk"):
        # print (x)
        risk.append(x)
all_d = []
count = 0
for x in risk:
    # print (x)
    d = elog[elog["action"]==x]
    u = []
    a = []
    r = []
    for x in d["body"].tolist():
        u.append(x["userID"])
        a.append(x["action"])
        try:
            r.append(x["reason"])
        except:
            r.append("")
    d1 = pd.DataFrame(u, columns=["user_id"])
    d1["action"] = a[0]
    d1["reason"] = r
    all_d.append(d1)
print ("Stage 2 completed")
all_d = pd.concat(all_d)
all_d.to_pickle("Events.pkl")
d2c_phone = d2c[d2c["user_id"].isin(all_d["user_id"].unique().tolist())][["user_id","phone_number" ]]
print (time.time() - start)



start = time.time()
all_d_phone = pd.merge(all_d,d2c_phone, on = "user_id", how = "left").fillna("No Phone number")
def phone_number_checker(phone_number):
    phone_number = str(phone_number)
    u = d2c[d2c["phone_number"]==phone_number]["user_id"].tolist()[-1]
    m = all_d_phone[all_d_phone["user_id"]==u]
    n = m[m["reason"]!=""][["user_id", "action","reason"]]
    if n.shape[0]>0:
        d3 = n["reason"].unique().tolist()
    else:
        d3 = []
    return d3
# print (phone_number_checker(9620126779))
ph = d2c["phone_number"].tolist()
ph_d = []
count = len(ph)
for x in ph:
    count-=1
    m = {}
    m[x] = phone_number_checker(x)
    ph_d.append(m)
    # print (count)
ph_d = {k:v for element in ph_d for k,v in element.items()}
d2c["phone"] = d2c["phone_number"]
d2c["phone"] = d2c["phone"].map(ph_d)
d2c.rename(columns={"phone":"Rejection Reason"},inplace=True)
print (time.time() - start)

os.chdir("..")
os.chdir("Outputs")
print ("Navigating back to the outputs to pick up all_rows.csv")

all_rows_1 = pd.read_csv("all_rows.csv")
os.chdir("..")
os.chdir("Code")

d2c_rows_1 = all_rows_1[all_rows_1["user_id"].isin(d2c["user_id"].tolist())]
d2c_rows_1 = d2c_rows_1[['user_id', 'paused',
       'monthly_salary', 'loan_agreement_number', 'tid',
       'Withdrawn Amount', 'disbursal(txn) date', 'Total Fees', 'Total Amount',
       'Total Fees Calculated', 'processing_fees', 'GST_fees',
       'Annual_income', 'overall_limit']]
d2c_rows_1["disbursal(txn) date"] = pd.to_datetime(d2c_rows_1["disbursal(txn) date"]).dt.date.astype(str)

d2c = pd.merge(d2c,d2c_rows_1, on = "user_id", how = "left")

print("Loan agreements table connected")

query = """select * from ems.loan_agreements la ;"""
loan_agreements = dataframe_generator(query)
loan_agreements = clean(loan_agreements)
loan_agreements=loan_agreements[["employee_id", "loan_agreement_number", "expiration_date", "path", "accepted", "accepted_at"]]
loan_agreements["expiration_date"] = pd.to_datetime(loan_agreements["expiration_date"]).dt.date
loan_agreements["today"] = pd.to_datetime("today")
loan_agreements["today"] = loan_agreements["today"].dt.date
loan_agreements["loan_duration"] = loan_agreements["expiration_date"] - loan_agreements["today"]
loan_agreements["loan_Closure_date"] = loan_agreements["expiration_date"]
loan_agreements["accepted_at"] = pd.to_datetime(loan_agreements["accepted_at"]).dt.date
loan_agreements.drop(["expiration_date","today"],1,inplace=True)
loan_agreements.rename(columns={"accepted_date":"loan_agreement_date"},inplace=True)
loan_agreements = loan_agreements[["employee_id", "accepted"]]
loan_agreements.rename(columns={"accepted":"Loan agreement accepted"},inplace=True)
d2c = pd.merge(d2c,loan_agreements, on = "employee_id", how ="left")



d2c_bq = bq_cleaner(d2c.copy())
pandas_gbq.to_gbq(d2c_bq, destination_table="Processed_data.D2C_Metrics_DataStudio", project_id="data-warehouse-india-copy", if_exists="replace")





print ("Uploading all D2C data to Sheet 15")
# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("data-warehouse-india-copy-5949defd88ea.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Copy of Data Studio Final Dashboard").worksheet("Sheet15")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(d2c.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time()-  start)


##NEW MODULE
print ("Dashboard Sheet - Marketing - D2CTotal - Daywise")

print ("Start of D2C-Total_Daywise to track D2C Withdrawing metrics only")
start = time.time()
query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
print (time.time() - start)

kyc = kyc[["user_id", "approved", "verified", "document_type", "created_at"]]

kyc = kyc[(kyc["approved"]==True)&(kyc["verified"]==True)]
kyc = kyc.groupby("user_id").last().reset_index()
kyc = kyc[["user_id", "created_at"]]
kyc.columns = ["user_id", "approved_kyc_date"]
d2c = pd.merge(d2c, kyc, on = "user_id", how = "left")


print ("perfios and table updation")
s1 = d2c.groupby("created_at").sum().reset_index()[["created_at", "Withdrawn Amount"]]
d2c_w = d2c[d2c["Withdrawn Amount"]>0]
uu={}
for x in s1["created_at"]:
    uu[x] = d2c_w[d2c_w["created_at"]==x]["user_id"].nunique()
uu = pd.DataFrame(uu.items())
uwf = d2c[(d2c["fraud"]=="Approved")&(d2c["underwriting"]=="Approved")]
perfios = {}
for x in s1["created_at"]:
    perfios[x] = uwf[uwf["created_at"]==x]["user_id"].nunique()
    
perfios = pd.DataFrame(perfios.items())
ba= d2c[(d2c["Bureau Approved"]=="Approved")]
bureau = {}
for x in s1["created_at"]:
    bureau[x] = ba[ba["created_at"]==x]["user_id"].nunique()
bureau = pd.DataFrame(bureau.items())
signups = {}
for x in s1["created_at"]:
    signups[x] = d2c[d2c["created_at"]==x]["user_id"].nunique()
signups = pd.DataFrame(signups.items())
kyc_d2c = d2c[d2c["kyc"]=="Approved"]
kyc = {}
for x in s1["created_at"]:
    kyc[x] = kyc_d2c[kyc_d2c["created_at"]==x]["user_id"].nunique()
kyc = pd.DataFrame(kyc.items())
uu.columns = ["created_at", "Unique users"]
perfios.columns = ["created_at", "Sum of perfios"]
signups.columns = ["created_at", "Phone_number"]
bureau.columns = ["created_at", "Bureau"]
kyc.columns = ["created_at", "KYC"]


s1 = pd.merge(s1,uu, on = "created_at", how = "left")
s1 = pd.merge(s1,perfios, on = "created_at", how = "left")
s1 = pd.merge(s1,signups, on = "created_at", how = "left")
s1 = pd.merge(s1,bureau, on = "created_at", how = "left")
s1 = pd.merge(s1,kyc, on = "created_at", how = "left")
m = d2c_w.groupby("created_at").count().reset_index()[["created_at", "phone_number"]]
m.columns = ["created_at", "Count of Withdrawals"]
s1 = pd.merge(s1,m, on = "created_at", how = "left").fillna(0)


s1["Signup to BU Approved"] = s1["Bureau"]/s1["Phone_number"]
s1["Bureau to Perfios Approved"] = s1["Sum of perfios"]/s1["Bureau"]
s1["Signup to Perfios"] = s1["Sum of perfios"]/s1["Phone_number"]
s1["Perfios to KYC"] = s1["KYC"]/s1["Sum of perfios"]
s1["KYC to Unique"] = s1["Unique users"]/s1["KYC"]

s1.replace([np.inf, -np.inf], np.nan, inplace=True)
s1 = s1.fillna("")
s1 = s1.round(decimals = 2)
s1.rename(columns={"Phone_number":"Signups", "Bureau":"Bureau_Approved", "KYC":"KYC_Approved", 
                  "Sum of perfios":"Perfios_Approved", "Count of Withdrawals":"No of txns"},inplace=True)








# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("data-warehouse-india-copy-5949defd88ea.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Copy of Data Studio Final Dashboard").worksheet("Sheet7")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(s1.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)
updated = bq_cleaner(s1.copy())
pandas_gbq.to_gbq(updated, destination_table="Processed_data.data_Studio_Sheet7", project_id="data-warehouse-india-copy", if_exists="replace")




al = d2c_w[["user_id", "lookup_name", "Total Amount"]]
a2 = al.groupby("lookup_name").count().reset_index()[["lookup_name", "user_id"]]
a2.columns = ["lookup_name", "users"]
a3 = al.groupby("lookup_name").sum().reset_index()
a4 = pd.merge(a2,a3,on = "lookup_name")
a4.columns = ["lookup_name", "Count of users", "Sum of Withdrawn amount"]

a4["Count of users"] = a4["Count of users"].astype(float)

# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("data-warehouse-india-copy-5949defd88ea.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Copy of Data Studio Final Dashboard").worksheet("Sheet17")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(a4.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)
updated = bq_cleaner(a4.copy())
pandas_gbq.to_gbq(updated, destination_table="Processed_data.data_studio_Sheet17", project_id="data-warehouse-india-copy", if_exists="replace")





print ("OTP Sheet update")

os.getcwd()
os.chdir("..")
os.chdir("otp_S3_Kinesis_Dump")

def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles


all_dumps = []
try:
    for x in getListOfFiles(os.getcwd()):
        if not x.endswith(".DS_Store"):
            # print (x)
            all_dumps.append(x)
except:pass
os.chdir("..")
os.chdir("Code")
ad = []
try:
    for x in all_dumps:
        y = pd.read_table(x, header=None)[0].tolist()
        y = ast.literal_eval(y[0])
        # print (y)
        ad.append(y)
except:pass

timestamps = []
delivery_number = []
sms_type = []
response = []
status = []

for x in ad:
    timestamps.append(x["notification"]["timestamp"])
    delivery_number.append(x["delivery"]["destination"])
    sms_type.append(x["delivery"]["smsType"])
    response.append(x["delivery"]["providerResponse"])
    status.append(x["status"])
    
final = pd.DataFrame()
final["timestamp"] = timestamps
final["Mobile number"] = delivery_number
final["sms_type"] = sms_type
final["Response"] = response
final["status"] = status
final["timestamp"] = pd.to_datetime(final["timestamp"])
final["timestamp"] = final["timestamp"].dt.tz_localize(pytz.utc).dt.tz_convert(my_timezone)
# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("data-warehouse-india-copy-5949defd88ea.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Copy of Data Studio Final Dashboard").worksheet("OTP")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(final.copy())
# # gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time()-start)

updated = bq_cleaner(final.copy())
pandas_gbq.to_gbq(updated, destination_table="Processed_data.OTP", project_id="data-warehouse-india-copy", if_exists="replace")



# print ("Installed Audience update")
# os.chdir("..")
# os.chdir("Installed Audience/")
# ia = pd.read_csv("All countries _ regions, India, Netherlands.csv")
# ia = ia[["Date", "Installed audience (All users, Unique users, Per interval, Daily): India"]]
# ia.columns = ["Date","Sum"]
# ia["Sum"] = ia["Sum"].str.replace(",", "").astype(int)
# ia["Total Installed till day"] = ia["Sum"] - ia["Sum"].shift(1)
# ia = ia.dropna()
# os.chdir("..")
# os.chdir("Code")










print ("Starting KYC_METRICS")
query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2
iam = iam[["user_id", "full_name", "status", "email", "phone_number", "created_at", "metadata"]]
gender=[]
birth_date=[]
for i in range(0,iam.shape[0]):
    gender.append(iam["metadata"].iloc[i]["gender"])
    birth_date.append(iam["metadata"].iloc[i]["birth_date"])

iam["Gender"] = gender
iam["birth_date"] = birth_date
iam.drop(["metadata"],1,inplace=True)
start = time.time()
query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)
print (time.time() - start)
ems_employees = ems_employees[["employee_id", "user_id", "employer_id"]]
start = time.time()
query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id","organization_id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
print (time.time() - start)

start = time.time()
xorg["organization_id"].replace({"c2a6a007-e625-456f-8c36-92cd2654c971": 'Quess',
                                              "916227f6-cb69-46ec-8cb1-a735ed98f2c4": 'D2C Org', 
                                             "d779558a-09cc-4920-9f39-d8409c8f0728":"B2B Test"},inplace=True)
print (time.time() - start)

ems_xorg= pd.merge(ems_employees, xorg, on = "employer_id")
iam_ems_xorg = pd.merge(iam, ems_xorg, on = "user_id", how = "left")
iam_ems_xorg = iam_ems_xorg[['user_id', 'created_at', 'organization_id','lookup_name', 'phone_number']]


query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)

cv.rename(columns={"score":"Approved"},inplace=True)

os.chdir("..")
os.chdir("AWS_Data/")
rootdir = os.getcwd()
files_dump =[]
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
#             print(os.path.join(subdir, file))
        if file.endswith("json"):
            files_dump.append(os.path.join(subdir, file))
ff = []
for x in files_dump:
    fff = {}
    f = open(x)
    try:
        data = json.load(f)
    except:
        data = "Json Failure - Issue at our AWS end"
    fff["user_id"] = str(f).split("/")[-2]
    fff["data"] = data
    ff.append(fff)
ffff = pd.DataFrame(ff)
os.chdir("..")
os.chdir("Code")

aws_approved = []
for x in ffff["data"]:
    if x=="Yes":
        aws_approved.append(True)
    else:
        aws_approved.append(False)
ffff["Aws Approved"] = aws_approved
cv = pd.merge(cv,ffff, on = "user_id", how = "left")


cv= pd.merge(cv, iam_ems_xorg[["user_id", "organization_id", "lookup_name"]], on = "user_id", how = "left")
cv = cv[["user_id", "Approved"]]
iam_ems_xorg = pd.merge(iam_ems_xorg, cv,on = "user_id",how = "left")










start = time.time()
query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)
print (time.time() - start)
uid = kyc["user_id"].unique().tolist()

selfie_only= []
selfie_and_pan = []
all_3=[]
for x in uid:
    d = kyc[kyc["user_id"]==x]["document_type"].value_counts().index.tolist()
    if "SILENTLIVENESS" in d:
        selfie_only.append(x)
    if "SILENTLIVENESS" in d and "PAN" in d:
        selfie_and_pan.append(x)
    if "SILENTLIVENESS" in d and "PAN" in d and "AADHAAR" in d:
        all_3.append(x)
print (time.time() - start)


stage_1 = pd.DataFrame(selfie_only, columns=["user_id"])
stage_1["Stage_1"] = True
stage_1

kyc = pd.merge(kyc,stage_1, on = "user_id", how = "left")

stage_2 = pd.DataFrame(selfie_and_pan, columns=["user_id"])
stage_2["Stage_2"] = True
stage_2

kyc = pd.merge(kyc,stage_2, on = "user_id", how = "left")

stage_3 = pd.DataFrame(all_3, columns=["user_id"])
stage_3["Stage_3"] = True
stage_3

kyc = pd.merge(kyc,stage_3, on = "user_id", how = "left")

kyc1 = kyc[["user_id", 'created_at', 'verified', 'document_type', 'Stage_1', 'Stage_2', 'Stage_3']]
kyc1 = kyc1.groupby("user_id").last().reset_index()
kyc1.columns = ["user_id", "kyc_hit_date", "verified", "document_type", "Stage_1", "Stage_2", "Stage_3"]
iam_ems_xorg = pd.merge(iam_ems_xorg, kyc1, on = "user_id", how = "left")
iam_ems_xorg['kyc_hit'] = np.where(iam_ems_xorg["kyc_hit_date"].notnull(), 'Yes', 'No')
def verified_flag(df):
    
    if (df['kyc_hit']=="No"):
        return 'No Hit'
    elif (df["verified"]==True):
        return "Karza Approved"
    elif (df["verified"]==False):
        return "Karza Rejected"
    

iam_ems_xorg['Karza Status'] = iam_ems_xorg.apply(verified_flag, axis = 1)
uid = iam_ems_xorg["user_id"].unique().tolist()
all_d = pd.read_pickle("Events.pkl")
all_d = all_d[all_d["action"]=="risk_verification.kyc_rejected"]
iam_ems_xorg = pd.merge(iam_ems_xorg,all_d, on = "user_id", how = "left")
reasons = iam_ems_xorg[iam_ems_xorg["reason"].notnull()]
reasons = reasons[["user_id", "reason"]]
reasons["reason"] = reasons["reason"].str.split(",").tolist()
m = pd.DataFrame(reasons["reason"].tolist())
reasons = reasons.reset_index(drop=True)
reasons = pd.concat([reasons,m],1)


pin_1 = []
pan_1 = []
aadhar_1 = []
voter_1 = []
reasons_1 = reasons[reasons[0].notnull()]
for x,y in zip(reasons_1["user_id"].tolist(), reasons_1[0].tolist()):
    pin_d = {}
    pan_d = {}
    aad_d = {}
    vot_d = {}
    if "PinCodeMismatch" in y:
        pin_d["user_id"] = x
        pin_1.append(pin_d)
    elif "PANNameMismatch" in y:
        pan_d["user_id"] = x
        pan_1.append(pan_d)
    elif "AadhaarNameMismatch" in y:
        aad_d["user_id"] = x
        aadhar_1.append(aad_d)
    elif "VoterIDNameMismatch" in y:
        vot_d["user_id"] = x
        voter_1.append(vot_d)
pin_2 = []
pan_2 = []
aadhar_2 = []
voter_2 = []
reasons_2 = reasons[reasons[1].notnull()]
for x,y in zip(reasons_2["user_id"].tolist(), reasons_2[1].tolist()):
    pin_d = {}
    pan_d = {}
    aad_d = {}
    vot_d = {}
    if "PinCodeMismatch" in y:
        pin_d["user_id"] = x
        pin_2.append(pin_d)
    elif "PANNameMismatch" in y:
        pan_d["user_id"] = x
        pan_2.append(pan_d)
    elif "AadhaarNameMismatch" in y:
        aad_d["user_id"] = x
        aadhar_2.append(aad_d)
    elif "VoterIDNameMismatch" in y:
        vot_d["user_id"] = x
        voter_2.append(vot_d)
        
pin_3 = []
pan_3 = []
aadhar_3 = []
voter_3 = []
reasons_3 = reasons[reasons[2].notnull()]
for x,y in zip(reasons_3["user_id"].tolist(), reasons_3[2].tolist()):
    pin_d = {}
    pan_d = {}
    aad_d = {}
    vot_d = {}
    if "PinCodeMismatch" in y:
        pin_d["user_id"] = x
        pin_3.append(pin_d)
    elif "PANNameMismatch" in y:
        pan_d["user_id"] = x
        pan_3.append(pan_d)
    elif "AadhaarNameMismatch" in y:
        aad_d["user_id"] = x
        aadhar_3.append(aad_d)
    elif "VoterIDNameMismatch" in y:
        vot_d["user_id"] = x
        voter_3.append(vot_d)
        
pin = pin_1+pin_2+pin_3
pan = pan_1+pan_2+pan_3
aadhar = aadhar_1+aadhar_2+aadhar_3
voter = voter_1+voter_2+voter_3

pin = pd.DataFrame(pin)
pin["Pincode mismatch"] = "Yes"
pan = pd.DataFrame(pan)
pan["PAN Mismatch"] = "Yes"
aadhar = pd.DataFrame(aadhar)
aadhar["Aadhaar mismatch"] = "Yes"
voter = pd.DataFrame(voter)
voter["Voter mismatch"] = "Yes"

reasons = pd.merge(reasons, pin, on = "user_id", how = "left")
reasons = pd.merge(reasons, pan, on = "user_id", how = "left")
reasons = pd.merge(reasons, voter, on = "user_id", how = "left")
reasons = pd.merge(reasons, aadhar, on = "user_id", how = "left")

reasons = reasons[["user_id", "Pincode mismatch", "PAN Mismatch", "Aadhaar mismatch", "Voter mismatch"]]
iam_ems_xorg = pd.merge(iam_ems_xorg, reasons, on = "user_id", how = "left")








iam_ems_xorg_bq = bq_cleaner(iam_ems_xorg)
pandas_gbq.to_gbq(iam_ems_xorg_bq, destination_table="Processed_data.KYC_Metrics_DataStudio", project_id="data-warehouse-india-copy", if_exists="replace")




# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("KYC_METRICS_DataStudio")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(iam_ems_xorg.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time() - start)




installed = iam_ems_xorg[["user_id","phone_number", "organization_id"]]
os.chdir("..")
os.chdir("Installed Audience/")

source = '/Users/ds-monk/Google Drive/Shared drives/Intercom/intercom_dump.csv'
destination = '/Users/ds-monk/Rainpay Code/all-india-dashboard-code/Installed Audience/intercom_dump.csv'
shutil.copy(source,destination)

intercom = pd.read_csv("intercom_dump.csv")
os.chdir("..")
os.chdir("Code")
intercom = intercom[["Last seen on Android (IST)", "Signed up (IST)", "Email", "Android sessions", "Phone", "phoneNumber", "First name"]]
intercom_phone = intercom[(intercom["phoneNumber"].notnull())&(intercom["Android sessions"]>1)]
intercom_phone.rename(columns={"phoneNumber":"phone_number"},inplace=True)
intercom_phone["phone_number"] = intercom_phone["phone_number"].astype(int).astype(str)
phone = []
for x in intercom_phone["phone_number"]:
    # print (x)
    phone.append(x)
phone = [x[2:] for x in phone]
intercom_phone["phone_number"] = phone
ia = installed[installed["phone_number"].isin(intercom_phone["phone_number"].unique().tolist())]
ia = pd.merge(ia, intercom_phone, on = "phone_number", how = "left")
print (ia.shape)


ia_bq = bq_cleaner(ia)
pandas_gbq.to_gbq(ia_bq, destination_table="Processed_data.Intercom_Installed_Data", project_id="data-warehouse-india-copy", if_exists="replace")






# start = time.time()
# scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
#             "https://www.googleapis.com/auth/drive"]
# creds = ServiceAccountCredentials.from_json_keyfile_name("warm-torus-341214-30e43816fb06.json", scope)
# client_2 = gspread.authorize(creds)
# employees_kyc_demographic= client_2.open("Data Studio Final Dashboard").worksheet("Sheet16")
# employees_kyc_demographic.clear()
# existing = gd.get_as_dataframe(employees_kyc_demographic)
# existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
# updated = existing.append(ia.copy())
# gd.set_with_dataframe(employees_kyc_demographic, updated)
# print (time.time()-start)









print ("Start Withdrawal status code")
query = """select * from bnk.transactions t  ;"""
txns_status = dataframe_generator(query)
txns_status = clean(txns_status)
print ("Capture all failed, pending and incomplete txns")
# txns = txns[txns["status"]!="COMPLETE"]
txns_status.rename(columns={"entity_id":"user_id"},inplace=True)
txns_status = txns_status[["user_id", "created_at","error_message", "status", "amount"]]

txns_bq = bq_cleaner(txns_status)
pandas_gbq.to_gbq(txns_bq, destination_table="Processed_data.Withdrawal_status_tracker", project_id="data-warehouse-india-copy", if_exists="replace")



print ("\n")
print ("Total Run Time: "+str(time.time() - start_1))





























