#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  6 13:10:58 2022

@author: arunabhmajumdar
"""

import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
start_1 = time.time()
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
print ("Changing directory to Code to aid ease of access to the various jsons and xlsx")
print(os.getcwd())
os.chdir("..")
time.sleep(2)
print ("Changed Directory")
print (os.getcwd())
start_1 = time.time()
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")



start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-production")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")

connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)



print (os.getcwd())

print ("Get Paused_Unpaused_data from Google Drive")


os.chdir("..")
os.chdir("..")
os.chdir("..")
os.chdir("Google Drive/")
os.chdir("Shared drives/")
os.chdir("Paused Unpaused/")
print (os.getcwd())
rootdir = os.getcwd()
files_dump =[]
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
#             print(os.path.join(subdir, file))
        if file.endswith(".xlsx"):
            files_dump.append(os.path.join(subdir, file))
              
            
all_dfs = []
for x in files_dump:
    df = pd.read_excel(x)
    all_dfs.append(df)
    
unpaused_paused = pd.concat(all_dfs)
unpaused_paused["status"] = unpaused_paused["status"].str.lower()
all_dates = []
for x in unpaused_paused["Date"].tolist():
    all_dates.append(x.strftime("%d-%B-%Y"))
unpaused_paused["Date"] = all_dates

print ("Changing back to code folder")
os.chdir("..")
os.chdir("..")
os.chdir("..")
os.chdir("..")
os.chdir("/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code/")



print (os.getcwd())

# while True:
print ("Starting upload")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df


paused_bq = bq_cleaner(unpaused_paused)
pandas_gbq.to_gbq(paused_bq, destination_table="Processed_data.paused_unpaused_history", project_id="data-warehouse-india", if_exists="replace")


# print ("Starting Event logs - Could take a while")
# start = time.time()
# query = """select body,action from elog.events e;"""
# elog = dataframe_generator(query)
# elog_bq = bq_cleaner(elog)
# pandas_gbq.to_gbq(elog_bq, destination_table="Raw_Postgres_data.elog_events", project_id="data-warehouse-india", if_exists="replace")
# print (time.time() - start)
# print ("elog events uploaded")



query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)

iam_users_bq = bq_cleaner(iam)
pandas_gbq.to_gbq(iam_users_bq, destination_table="Raw_Postgres_data.iam_users", project_id="data-warehouse-india", if_exists="replace")
print ("iam uploaded")

query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = clean(ems_employees)

ems_employees_bq = bq_cleaner(ems_employees)
pandas_gbq.to_gbq(ems_employees_bq, destination_table="Raw_Postgres_data.ems_employees", project_id="data-warehouse-india", if_exists="replace")
print ("ems uploaded")

query = """select * from ems.compensations c ;"""
compensations = dataframe_generator(query)
compensations = clean(compensations)

compensations_bq = bq_cleaner(compensations)
pandas_gbq.to_gbq(compensations_bq, destination_table="Raw_Postgres_data.ems_compensations", project_id="data-warehouse-india", if_exists="replace")

print ("ems compensations uploaded")
query = """select * from ems.loan_agreements la ;"""
loan_agreements = dataframe_generator(query)
loan_agreements = clean(loan_agreements)

loan_agreements_bq = bq_cleaner(loan_agreements)
pandas_gbq.to_gbq(loan_agreements_bq, destination_table="Raw_Postgres_data.ems_loan_agreements", project_id="data-warehouse-india", if_exists="replace")

print ("ems loan agreements uploaded")
query = """select * from bnk.transactions t  ;"""
txns = dataframe_generator(query)

bnk_transactions_bq = bq_cleaner(txns)
pandas_gbq.to_gbq(bnk_transactions_bq, destination_table="Raw_Postgres_data.bnk_transactions", project_id="data-warehouse-india", if_exists="replace")

print ("bnk transactions uploaded")
query = """select * from kbill.employer_invoice_deductions eid ;"""
eid = dataframe_generator(query)
eid = clean(eid)

eid_bq = bq_cleaner(eid)
pandas_gbq.to_gbq(eid_bq, destination_table="Raw_Postgres_data.kbill_employer_invoice_deductions", project_id="data-warehouse-india", if_exists="replace")
print ("kbill employer invoice deductions uploaded")

query = """select * from xorg.configurations xc ;"""
xc= dataframe_generator(query)
xc_bq = bq_cleaner(xc)
pandas_gbq.to_gbq(xc_bq, destination_table="Raw_Postgres_data.xorg_configurations", project_id="data-warehouse-india", if_exists="replace")

print ("xorg configurations uploaded")
query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg_bq = bq_cleaner(xorg)
pandas_gbq.to_gbq(xorg_bq, destination_table="Raw_Postgres_data.xorg_employers", project_id="data-warehouse-india", if_exists="replace")

print ("xorg employers uploaded")
query = """select * from bnk.external_accounts ea ;"""
external_accounts = dataframe_generator(query)
external_accounts = clean(external_accounts)

ea_bq = bq_cleaner(external_accounts)
pandas_gbq.to_gbq(ea_bq, destination_table="Raw_Postgres_data.bnk_external_accounts", project_id="data-warehouse-india", if_exists="replace")
print ("bnk external accounts uploaded")

query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)

kyc_bq = bq_cleaner(kyc)
pandas_gbq.to_gbq(kyc_bq, destination_table="Raw_Postgres_data.kyc_documents", project_id="data-warehouse-india", if_exists="replace")
print ("kyc documents uploaded")
query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)

cv_bq = bq_cleaner(cv)
pandas_gbq.to_gbq(cv_bq, destination_table="Raw_Postgres_data.risk_user_risk_verifications", project_id="data-warehouse-india", if_exists="replace")
print ("risk user risk verifications uploaded")

query = """select * from ems.work_period_balances wpb ;"""
wpb = dataframe_generator(query)
wpb = clean(wpb)
wpb_bq = bq_cleaner(wpb)
pandas_gbq.to_gbq(wpb_bq, destination_table="Raw_Postgres_data.work_period_balances", project_id="data-warehouse-india", if_exists="replace")
print ("work period balances uploaded")


print (os.getcwd())



print ("\n")
print ("Total Run Time: "+str(time.time() - start_1))


