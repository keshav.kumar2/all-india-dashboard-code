#!/bin/bash

CIPHER=$1

PLAIN_TEXT=$(aws kms decrypt --key-id alias/prod/encryption-key \
  --ciphertext-blob "$CIPHER" --output text --query Plaintext | base64 --decode
)

echo $PLAIN_TEXT
