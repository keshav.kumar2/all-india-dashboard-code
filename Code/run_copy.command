#!/bin/sh

echo "Opening a New Terminal and SSH through it to the bastion server"
screen -dmS "Frog" bash -c 'ssh -i /Users/ds-monk/Rainpay\ Code/all-india-dashboard-code/Code/prod-bastion-india_replica.pem ec2-user@13.233.106.82 -p 22 -L 55432:read-replica-prod-rds-india.ctzcckgcc8qw.ap-south-1.rds.amazonaws.com:5432'

echo "Bastion server is up at ec2-user@13.233.106.82."
echo "Now you can connect to rain-inda-production database easily at port 55432"

/usr/local/bin/aws sso login --profile rain-india-production

echo "Starting Google Drive Update"


echo "changing directory to home"
cd
pwd


echo " Starting delta pull from S3 bucket to local folders."

echo "Running AWS_Quess_Landing_Page S3 Pull"
/usr/local/bin/aws s3 sync s3://quess-landing-page /Users/ds-monk/Rainpay\ Code/all-india-dashboard-code/AWS_Quess_Landing_Page --profile rain-india-production 



echo "Running OTP Kinesis S3 pull"
/usr/local/bin/aws s3 sync s3://cw-kinesis-s3-replica /Users/ds-monk/Rainpay\ Code/all-india-dashboard-code/otp_S3_Kinesis_Dump --profile rain-india-production 



echo "Loan Agreement S3 pull"
/usr/local/bin/aws s3 sync s3://loan-agreements.in /Users/ds-monk/Rainpay\ Code/all-india-dashboard-code/Agreements --profile rain-india-production 


echo "AWS Data S3 pull"
/usr/local/bin/aws s3 sync s3://india.kyc.rain.us /Users/ds-monk/Rainpay\ Code/all-india-dashboard-code/AWS_Data --profile rain-india-production 

echo " S3 Data Pull Completed."

echo " Starting code execution for data processing.   "

echo " Changing Directory to Actual Codes to be used."
cd Rainpay\ Code/all-india-dashboard-code/Code/Actual\ Codes\ to\ be\ used

echo " Running Withdrawals_without_Bank_prood.py script."
/Users/ds-monk/opt/anaconda3/bin/python Withdrawals_without_Bank_prod.py

echo " Running Data_Studio_1_prod.py script."
/Users/ds-monk/opt/anaconda3/bin/python Data_Studio_1_prod.py

echo " Running Data_Studio_2_prod.py script."
/Users/ds-monk/opt/anaconda3/bin/python Data_Studio_2_prod.py

