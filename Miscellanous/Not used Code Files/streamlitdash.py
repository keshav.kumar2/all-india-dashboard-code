# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy as np
import streamlit as st
import os

#Changing directory to outputs where csv outputs for withdrawals are stored
os.chdir("..")
os.chdir("Outputs")
df = pd.read_csv("all_rows.csv")
df.drop(["Unnamed: 0"],1,inplace = True)
df["disbursal(txn) date"] = pd.to_datetime(df["disbursal(txn) date"]).dt.date
os.chdir("..")
os.chdir("Code")




disbursal_date = df['disbursal(txn) date'].unique()
# years = df['year']
# models = df['model']
# engines = df['engine']
# components = df['components']
date_choice = st.sidebar.selectbox('Select your date:', disbursal_date)
# year_choice = st.sidebar.selectbox('', years)
# model_choice = st.sidebar.selectbox('', models)
# engine_choice = st.sidebar.selectbox('', engines)


st.header("All Withdrawals")

st.write('Results:', df[df["disbursal(txn) date"]==date_choice])

@st.cache
def convert_df_to_csv(df):
  # IMPORTANT: Cache the conversion to prevent computation on every rerun
  return df.to_csv().encode('utf-8')


st.download_button(
  label="Download data as CSV",
  data=convert_df_to_csv(df[df["disbursal(txn) date"]==date_choice]),
  file_name='txns_file.csv',
  mime='text/csv',
)