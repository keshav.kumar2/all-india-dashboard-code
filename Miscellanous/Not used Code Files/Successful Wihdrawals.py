#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 09:34:25 2021

@author: arunabhmajumdar
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 09:34:25 2021

@author: arunabhmajumdar
"""
import os
import psycopg2
import gspread
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import time



connection = psycopg2.connect(user="rainadmin",
                                  password="Mudar123",
                                  host="localhost",
                                  port=55432,
                                  database="rain")

    # Create a cursor to perform database operations
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")

cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)


ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
       'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]

ems_employees = ems_employees.rename(columns={'id': 'employee_id'})

query = """select * from iam.users u ;"""
iam_users = dataframe_generator(query)
iam_users = iam_users.rename(columns={'id': 'user_id'})
iam_users = iam_users[["user_id", "full_name", "email","phone_number", "metadata","document_number"]]
iam_ems_employees = pd.merge(ems_employees,iam_users, on = "user_id")

query = """select * from kyc.documents d ;"""
kyc = dataframe_generator(query) 
kyc = kyc[["user_id","file_extension", "document_type", "side", "verified", "approved"]]
iam_ems_kyc_employees = pd.merge(iam_ems_employees, kyc, on = "user_id")

gender=[]
birth_date=[]
for i in range(0,iam_ems_kyc_employees.shape[0]):
    gender.append(iam_ems_kyc_employees["metadata"].iloc[i]["gender"])
    birth_date.append(iam_ems_kyc_employees["metadata"].iloc[i]["birth_date"])
    
iam_ems_kyc_employees["Gender"] = gender
iam_ems_kyc_employees["birth_date"] = birth_date
iam_ems_kyc_employees.drop(["metadata"],1,inplace=True)
iam_ems_kyc_employees = iam_ems_kyc_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',
'organization_id','document_type', 'side','document_number', 'file_extension', 'verified', 'approved']]
       
query = """select * from ems.compensations c ;"""
compensations = dataframe_generator(query)
compensations = compensations[["employee_id", "monthly_salary"]]
compensations["monthly_salary"] = compensations["monthly_salary"]/100
rows_1_39 = pd.merge(iam_ems_kyc_employees,compensations, on = "employee_id")


query = """select * from ems.loan_agreements la ;"""
loan_agreements = dataframe_generator(query)
loan_agreements=loan_agreements[["employee_id", "loan_agreement_number", "expiration_date", "path", "accepted", "accepted_at"]]
loan_agreements["expiration_date"] = pd.to_datetime(loan_agreements["expiration_date"]).dt.date
loan_agreements["today"] = pd.to_datetime("today")
loan_agreements["today"] = loan_agreements["today"].dt.date
loan_agreements["loan_duration"] = loan_agreements["expiration_date"] - loan_agreements["today"]
loan_agreements["loan_Closure_date"] = loan_agreements["expiration_date"]
loan_agreements["accepted_at"] = pd.to_datetime(loan_agreements["accepted_at"]).dt.date
loan_agreements.drop(["expiration_date","today"],1,inplace=True)
loan_agreements.rename(columns={"accepted_date":"loan_agreement_date"},inplace=True)

all_rows = pd.merge(rows_1_39,loan_agreements,on = "employee_id")


query = """select * from kbill.employer_invoice_deductions eid ;"""
kbill = dataframe_generator(query)
kbill = kbill[["employee_id", "amount"]]
kbill.rename(columns={"amount":"Total amount"}, inplace=True)
kbill["Total amount"] = kbill["Total amount"]/100
all_rows = pd.merge(all_rows, kbill, on = "employee_id")

query = """select * from bnk.transactions t  ;"""
txns = dataframe_generator(query)
txns.rename(columns={"entity_id":"user_id"},inplace=True)
txns.rename(columns={"amount":"Withdrawn Amount"}, inplace=True)
txns = txns[["user_id", "Withdrawn Amount"]]
txns["Withdrawn Amount"] = txns["Withdrawn Amount"]/100
all_rows = pd.merge(all_rows, txns, on = "user_id")
all_rows["total_fees"] = all_rows["Total amount"] - all_rows["Withdrawn Amount"]
all_rows["processing_fees"] = round(all_rows["total_fees"]/1.18,2)
all_rows["GST_fees"] = all_rows["total_fees"] - all_rows["processing_fees"]
all_rows.drop(["total_fees"],1,inplace = True)
all_rows["Annual_income"] = (all_rows["monthly_salary"]*12).astype(float)
all_rows["Annual_income"] = all_rows["Annual_income"].astype(str)


query = """select * from ems.work_period_balances wpb ;"""
wpb = dataframe_generator(query)
wpb = wpb[["employee_id", "available_amount", "payment_date"]]
wpb.rename(columns={"available_amount":"overall_limit", "payment_date":"next_payment_date"},inplace=True)
wpb["next_payment_date"] = pd.to_datetime(wpb["next_payment_date"]).dt.date
all_rows = pd.merge(all_rows, wpb, on = "employee_id")

os.chdir("..")
os.chdir("Outputs")
all_rows = all_rows.drop_duplicates(["user_id"])
all_rows.to_csv("all_rows.csv")
os.chdir("..")
os.chdir("Code")


scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
        "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client = gspread.authorize(creds)

os.chdir("..")
os.chdir("Outputs")

df = pd.read_csv('all_rows.csv')
employees_kyc_demographic= client.open("CS/OPS Dashboard").worksheet("Successful Withdrawals")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(df)
gd.set_with_dataframe(employees_kyc_demographic, updated)

os.chdir("..")
os.chdir("Code")