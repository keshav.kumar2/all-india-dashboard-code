#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 17 22:27:56 2021

@author: arunabhmajumdar
"""

import os
print (os.getcwd())
import pandas as pd,os
os.chdir("Repayment received till 17102021/")

refund_data = pd.read_excel("Repayment received through Quess.xlsx")
refund_data = refund_data[["user_id", "Due date", "repayment"]]
refund_data_2 = pd.read_excel("Repayment received on payu dash.xlsx")
refund_data_2 = refund_data_2.dropna()
refund_data_2 = refund_data_2[["user_id", "Due date", "repayment"]]
total_refunds = pd.concat([refund_data, refund_data_2])
# total_refunds = total_refunds.drop_duplicates("user_id")
total_refunds.rename(columns={"Due date":"created at"},inplace=True)
total_refunds["created at"] = total_refunds["created at"].astype(str)

total_refunds = total_refunds[["user_id", "created at", "repayment"]]
os.chdir("..")
print (os.getcwd())




import base64
import boto3
import pandas as pd
import os
import time
import warnings
warnings.filterwarnings("ignore")
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
import numpy as np


import time
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')


print ("end of Stage 1: Ground control to Major Tom")






print ("ok")
#     time.sleep(3)
print ("Dynamo Update Landing Page")
#     time.sleep(120)
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-production")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
# table = dynamodb.Table("QuessLandingDropoff")
response = client.scan(TableName = "QuessLandingDropoff")
df = pd.DataFrame(response["Items"])
df= df[["employee_id","phone_number","status","created_at","updated_at"]]
employee_id = []
phone_number = []
status = []
created_at = []
updated_at = []
for x in range(df.shape[0]):
    employee_id.append(df["employee_id"].iloc[x]["S"])
    phone_number.append(df["phone_number"].iloc[x]["S"])
    status.append(df["status"].iloc[x]["S"])
    created_at.append(df["created_at"].iloc[x]["S"])
    updated_at.append(df["updated_at"].iloc[x]["S"])


print (len(employee_id))
print (len(phone_number))
print (len(status))
print (len(created_at))
print (len(updated_at))



df["employee_id"] = employee_id
df["phone_number"] = phone_number
df["status"] = status
df["created_at"] = created_at
df["updated_at"] = updated_at
df = df.sort_values("created_at")

scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
        "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("CS/OPS Dashboard").worksheet("New Quess Landing Page (updated)")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(df)
gd.set_with_dataframe(employees_kyc_demographic, updated)

print ("Starting Funnel")





df["created_at"] = pd.to_datetime(df["created_at"]).dt.tz_convert(my_timezone)
df["created_at"] = pd.to_datetime(df["created_at"]).dt.date.astype(str)
df = df.set_index("created_at")
df = df.loc["2021-09-01":]
df["phone_number"] = df["phone_number"].astype(str)
phone_number = []
for x in df["phone_number"]:
    if len(x)==10:
        phone_number.append(x)
    else:
        phone_number.append(x[2:])
df["phone_number"] = phone_number
#     df["phone_number"] = df["phone_number"].astype(int)
df["phone_number"] = df["phone_number"].astype(str)
existing_2 = df.reset_index()




connection = psycopg2.connect(user="rainadmin",
                                  password="Mudar123",
                                  host="localhost",
                                  port=55432,
                                  database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")

cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df

print ("Table 1: IAM")

query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2


print ("Tbale 2: cv")

query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)

cv.rename(columns={"score":"Approved"},inplace=True)

os.chdir("..")
os.chdir("AWS_Data/")
rootdir = os.getcwd()
files_dump =[]
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
#             print(os.path.join(subdir, file))
        if file.endswith("json"):
            files_dump.append(os.path.join(subdir, file))
ff = []
for x in files_dump:
    fff = {}
    f = open(x)
    try:
        data = json.load(f)
    except:
        data = "Json Failure - Issue at our AWS end"
    fff["user_id"] = str(f).split("/")[-2]
    fff["data"] = data
    ff.append(fff)
ffff = pd.DataFrame(ff)


aws_approved = []
for x in ffff["data"]:
    if x=="Yes":
        aws_approved.append(True)
    else:
        aws_approved.append(False)
ffff["Aws Approved"] = aws_approved
cv = pd.merge(cv,ffff, on = "user_id")

keys = cv["data"].value_counts().index.tolist()
values = cv["data"].value_counts().values.tolist()

ll = pd.DataFrame(dict(zip(keys,values)).items())
keys_2 = [x.rstrip() for x in keys]
ll[0] = keys_2
lll = ll.groupby([0]).sum()
lll = lll.reset_index()
keys = lll[0].tolist()
values = lll[1].tolist()
inds_ = dict(zip(keys,values))


k =[]
v = []
m = []
n = []
for x in inds_.items():
    if x[0]=="No":
        k.append("Rejected due to Low Score")
        v.append(x[1])
    elif x[0]=="Yes":
        k.append("Original Bureau Approved")
        v.append(x[1])
    else:
        m.append(x[0])
        n.append(x[1])
inds_ = dict(zip(k,v))
inds_["Rejected due to other reasons"] = np.sum(n)


os.chdir("..")
os.chdir("Code")
play_store = pd.read_csv("All countries _ regions, India, Netherlands.csv")
play_store = play_store.iloc[:,:2]
from datetime import datetime
dates = play_store["Date"].tolist()
new_dates = []
for x in dates:
    new_dates.append(datetime.strptime(x, '%b %d, %Y'))
new_dates = [x.strftime("%Y-%m-%d") for x in new_dates]
play_store["Date"] = new_dates
play_store.columns = ["Date", "Installed Base"]
play_store = play_store[play_store["Date"]>"2021-08-30"]


response = client.scan(TableName = "prod-auth-token")
df_1= pd.DataFrame(response["Items"])

access_token = []
time_to_live = []
id_ = []
for x in range(df_1.shape[0]):
    access_token.append(df_1["AccessToken"].iloc[x]["S"])
    time_to_live.append(df_1["TimeToLive"].iloc[x]["N"])
    id_.append(df_1["ID"].iloc[x]["S"])

df_1["AccessToken"] = access_token
df_1["TimeToLive"] = time_to_live
df_1["ID"] = id_

ttl = df_1["TimeToLive"].astype(int).tolist()

ttl = [datetime.fromtimestamp(x).strftime("%Y-%m-%d %H:%M:%S") for x in ttl]

df_1["TimeToLive"]=ttl
df_1["created_at"] = pd.to_datetime(df_1["TimeToLive"]).dt.date.astype(str)



query = """select * from kyc.documents kyc ;"""
kyc = dataframe_generator(query)
kyc = clean(kyc)

kyc = kyc[kyc["approved"]==True]

uid = kyc["user_id"].unique().tolist()

selfie_only= []
selfie_and_pan = []
all_3=[]
for x in uid:
    d = kyc[kyc["user_id"]==x]["document_type"].value_counts().index.tolist()
    if "SILENTLIVENESS" in d:
        selfie_only.append(x)
    if "SILENTLIVENESS" in d and "PAN" in d:
        selfie_and_pan.append(x)
    if "SILENTLIVENESS" in d and "PAN" in d and "AADHAAR" in d:
        all_3.append(x)
        
        
query = """select * from bnk.external_accounts ea ;"""
bnk_external = dataframe_generator(query)
bnk_external = clean(bnk_external)



query = """select * from ems.loan_agreements la ;"""
la = dataframe_generator(query)
la = clean(la)
la = la[1:]



query = """select * from bnk.transactions t  ;"""
txns_2 = dataframe_generator(query)
txns_2 = clean(txns_2)
txns_2.rename(columns={"entity_id":"user_id"},inplace=True)
txns_2.rename(columns={"amount":"Withdrawn Amount"}, inplace=True)
# txns.rename(columns={"created_at":"disbursal(txn) date"},inplace=True)
txns_2 = txns_2[txns_2["status"]=="COMPLETE"]
txns_2 = txns_2[["id","user_id", "Withdrawn Amount","status", "created_at"]]
txns_2["Withdrawn Amount"] = txns_2["Withdrawn Amount"]/100
txns_2.rename(columns={"id":"tid"},inplace=True)








final = {}
fields = ["Click on Rain Banner inside on Dash App",
"Click on Product despcription Page",
"User lands on Rain signup page",
"Users landing on Password screen(password not yet created)",
"User is created(Password has been set)",
"Bureau Pull (Total attempts)"
         ]
for x in fields:
    final[x] = ""



final["User lands on Rain signup page"] = df["phone_number"].nunique()
final["User is created(Password has been set)"] = iam.shape[0]
final["Bureau Pull (Total attempts)"] = cv["user_id"].nunique()
final.update(inds_)
final["Overridden Bureau Approved"] = cv[cv["Approved"]==True]["user_id"].nunique()
final["User reaches OTP Screen"] = iam.shape[0]
final["User enters correct OTP"] = iam['status'].value_counts()[0]
final["Total Installed Base"] = play_store["Installed Base"].iloc[-1]
final["Total Signins"] = df_1.shape[0]
final["KYC_Stage"] = kyc["user_id"].nunique()
final["KYC Stage 1: Selfie approved"] = len(selfie_only)
final["KYC Stage 2: Selfie and PAN approved"] = len(selfie_and_pan)
final["KYC Stage 3: All 3 approved"] = len(all_3)
final["KYC_XML"] = kyc[(kyc["document_type"]=="AADHAAR")&(kyc["side"]=="")]["user_id"].nunique()
final["KYC_OCR"] = final["KYC Stage 3: All 3 approved"] - final["KYC_XML"]
final["User completes penny drop"] =bnk_external[bnk_external["status"]=="ACTIVE"].shape[0]
final["Loan Agreement Accepted"] = la.shape[0]
final["Total withdrawing users"] = txns_2["user_id"].nunique()
final["Total Amount Withdrawn"] = txns_2["Withdrawn Amount"].sum()
final["Total withdrawals"] = txns_2.shape[0]
final = pd.DataFrame(final.items())
final.columns = ["Funnel Stage", "Total till date"]

for x in final.columns.tolist():
    final[x] = final[x].astype(str)
    
    
    
    
    
final_new = {}
date = "2021-09-02"
fields = ["Click on Rain Banner inside on Dash App",
"Click on Product despcription Page",
"User lands on Rain signup page",
"Users landing on Password screen(password not yet created)",
"User is created(Password has been set)",
"Bureau Pull (Total attempts)"
          ]
for x in fields:
    final_new[x] = ""

existing_2_new = existing_2[existing_2["created_at"]==date]
iam_new = iam[iam["created_at"]==date]
cv_new = cv[cv["created_at"]==date]
df_1_new = df_1[df_1["created_at"]==date]

final_new["User lands on Rain signup page"] = existing_2_new["phone_number"].nunique()
final_new["User is created(Password has been set)"] = iam_new.shape[0]
final_new["Bureau Pull (Total attempts)"] = cv_new["user_id"].nunique()
keys_new = cv_new["data"].value_counts().index.tolist()
values_new = cv_new["data"].value_counts().values.tolist()
ll_new = pd.DataFrame(dict(zip(keys_new,values_new)).items())
keys_2_new = [x.rstrip() for x in keys_new]
ll_new[0] = keys_2_new
lll_new = ll_new.groupby([0]).sum()
lll_new = lll_new.reset_index()
keys_new = lll_new[0].tolist()
values_new = lll_new[1].tolist()
inds_new= dict(zip(keys_new,values_new))

   
        
        
k_new =[]
v_new = []
m_new = []
n_new = []
for x in inds_new.items():
    if x[0]=="No":
        k_new.append("Rejected due to Low Score")
        v_new.append(x[1])
    elif x[0]=="Yes":
        k_new.append("Original Bureau Approved")
        v_new.append(x[1])
    else:
        m_new.append(x[0])
        n_new.append(x[1])
inds_new = dict(zip(k_new,v_new))
inds_new["Rejected due to other reasons"] = np.sum(n_new)
print (inds_new)

# inds_new = {} 
# for x in inds_.items():
#     try:
#         inds_new[x[0]] = inds_new[x[1]]
#     except:
#         inds_new[x[0]] = inds_new[x[1]]


final_new.update(inds_new)
final_new["Overridden Bureau Approved"] = cv_new[cv_new["Approved"]==True]["user_id"].nunique()
final_new["User reaches OTP Screen"] = iam_new.shape[0]
final_new["User enters correct OTP"] = iam_new['status'].value_counts()[0]
m = play_store[play_store["Date"]==date]["Installed Base"].values.tolist()[-1]
n = play_store[play_store["Date"]<date]["Installed Base"].iloc[-1]
final_new['Total Installed Base'] = m-n

final_new["Total Signins"] = df_1_new.shape[0]
# # final["KYC_Stage"] = kyc["user_id"].nunique()
# # final["KYC Stage 1: Selfie approved"] = len(selfie_only)
# # final["KYC Stage 2: Selfie and PAN approved"] = len(selfie_and_pan)
# # final["KYC Stage 3: All 3 approved"] = len(all_3)
# # final["KYC_XML"] = kyc[(kyc["document_type"]=="AADHAAR")&(kyc["side"]=="")]["user_id"].nunique()
# # final["KYC_OCR"] = final["KYC Stage 3: All 3 approved"] - final["KYC_XML"]
# # final["User completes penny drop"] =bnk_external[bnk_external["status"]=="ACTIVE"].shape[0]
# # final["Loan Agreement Accepted"] = la.shape[0]
# # final["Total withdrawing users"] = txns_2["user_id"].nunique()
# # final["Total Amount Withdrawn"] = txns_2["Withdrawn Amount"].sum()
# # final["Total withdrawals"] = txns_2.shape[0]
final_new = pd.DataFrame(final_new.items())
final_new.columns = ["Funnel Stage", date]

for x in final_new.columns.tolist():
    final_new[x] = final_new[x].astype(str)



















# from datetime import timedelta, date

# def daterange(date1, date2):
#     for n in range(int ((date2 - date1).days)+1):
#         yield date1 + timedelta(n)

# start_dt = date(2021, 9, 1)
# # end_dt = date.today()
# end_dt = date(2021, 9, 30)
# dates = []
# for dt in daterange(start_dt, end_dt):
#     dates.append(dt.strftime("%Y-%m-%d"))












final_combined = pd.merge(final,final_new, on = "Funnel Stage", how = "outer").fillna(np.nan)




employees_kyc_demographic= client_2.open("CS/OPS Dashboard").worksheet("Funnel Data Testing")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(final_combined.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)



    
