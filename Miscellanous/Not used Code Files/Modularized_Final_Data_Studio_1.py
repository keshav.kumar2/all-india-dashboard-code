{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ef0891e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "import pandas as pd,os\n",
    "import base64\n",
    "import boto3\n",
    "import time\n",
    "import pandas as pd, os\n",
    "from oauth2client.service_account import ServiceAccountCredentials\n",
    "import gspread\n",
    "import gspread_dataframe as gd\n",
    "import psycopg2\n",
    "import df2gspread as d2g\n",
    "import pandas as pd\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "import numpy as np\n",
    "import re\n",
    "import calendar\n",
    "import os\n",
    "import json\n",
    "import time\n",
    "import numpy as np\n",
    "import pytz\n",
    "my_timezone = pytz.timezone('Asia/Calcutta')\n",
    "import ast\n",
    "import gzip\n",
    "from datetime import timedelta"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "e23a6141",
   "metadata": {},
   "outputs": [],
   "source": [
    "scope = [\"https://www.googleapis.com/auth/spreadsheets\", \"https://www.googleapis.com/auth/drive.file\", \n",
    "            \"https://www.googleapis.com/auth/drive\"]\n",
    "creds = ServiceAccountCredentials.from_json_keyfile_name(\"cs-and-ops-dashboard-8febbecf58a8.json\", scope)\n",
    "client_2 = gspread.authorize(creds)\n",
    "employees_kyc_demographic= client_2.open(\"CS/OPS Dashboard\").worksheet(\"New Quess Landing Page (updated)\")\n",
    "existing = gd.get_as_dataframe(employees_kyc_demographic)\n",
    "existing = pd.DataFrame(employees_kyc_demographic.get_all_records())\n",
    "existing[\"created_at\"] = pd.to_datetime(existing[\"created_at\"]).dt.date\n",
    "landing_page = existing.copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "9475e06e",
   "metadata": {},
   "outputs": [],
   "source": [
    "scope = [\"https://www.googleapis.com/auth/spreadsheets\", \"https://www.googleapis.com/auth/drive.file\", \n",
    "            \"https://www.googleapis.com/auth/drive\"]\n",
    "creds = ServiceAccountCredentials.from_json_keyfile_name(\"cs-and-ops-dashboard-8febbecf58a8.json\", scope)\n",
    "client_2 = gspread.authorize(creds)\n",
    "employees_kyc_demographic= client_2.open(\"Data Studio Final Dashboard\").worksheet(\"Sheet3\")\n",
    "employees_kyc_demographic.clear()\n",
    "existing = gd.get_as_dataframe(employees_kyc_demographic)\n",
    "existing = pd.DataFrame(employees_kyc_demographic.get_all_records())\n",
    "updated = existing.append(landing_page.copy())\n",
    "gd.set_with_dataframe(employees_kyc_demographic, updated)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "6964101b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(29376, 5)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "landing_page.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "af509576",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code'"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "os.getcwd()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "d532fe3e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code\n",
      "/Users/arunabhmajumdar\n",
      "Today's date: 2021-12-21\n",
      "/Volumes/GoogleDrive/Shared drives/Repayments/21-Dec-2021\n",
      "/Volumes/GoogleDrive/Shared drives/Repayments/21-Dec-2021\n"
     ]
    }
   ],
   "source": [
    "print (os.getcwd())\n",
    "os.chdir(\"..\")\n",
    "os.chdir(\"..\")\n",
    "os.chdir(\"..\")\n",
    "print (os.getcwd())\n",
    "\n",
    "os.chdir(\"Google Drive/\")\n",
    "os.chdir(\"Shared drives/\")\n",
    "os.chdir(\"Repayments/\")\n",
    "\n",
    "\n",
    "\n",
    "from datetime import date\n",
    "from datetime import timedelta\n",
    "today = date.today()\n",
    "# yest = today-timedelta(days =3)\n",
    "print(\"Today's date:\", today)\n",
    "today = today.strftime(\"%d-%b-%Y\")\n",
    "\n",
    "os.chdir(today)\n",
    "print (os.getcwd())\n",
    "# pd.read_excel(os.listdir()[0])\n",
    "total_refunds_2_oct= pd.read_excel((os.listdir()[0]), sheet_name=\"Oct\")\n",
    "total_refunds_2_nov= pd.read_excel((os.listdir()[0]), sheet_name=\"Nov\")\n",
    "total_refunds_2_dec= pd.read_excel((os.listdir()[0]), sheet_name=\"Dec\")\n",
    "total_refunds_2 = pd.concat([total_refunds_2_oct,total_refunds_2_nov, total_refunds_2_dec])\n",
    "total_refunds_2.rename(columns={\"Due date\":\"created at\"},inplace=True)\n",
    "total_refunds_2.drop([\"full_name\"],1,inplace=True)\n",
    "total_refunds = total_refunds_2.copy()\n",
    "total_refunds[\"created at\"] = total_refunds[\"created at\"].astype(str)\n",
    "print (os.getcwd())\n",
    "os.chdir(\"/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "1e11bdd3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8694811.07"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "total_refunds_2[\"repayment\"].sum()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "f2516db2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# total_refunds_2_oct= pd.read_excel(\"Repayment received till 16122021.xlsx\", sheet_name=\"Oct\")\n",
    "# total_refunds_2_nov= pd.read_excel(\"Repayment received till 16122021.xlsx\", sheet_name=\"Nov\")\n",
    "# total_refunds_2_dec= pd.read_excel(\"Repayment received till 16122021.xlsx\", sheet_name=\"Dec\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "9c0a5a89",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>user_id</th>\n",
       "      <th>full_name</th>\n",
       "      <th>repayment</th>\n",
       "      <th>Due date</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>61d3dc55-3321-4a65-b6b4-b415d0202030</td>\n",
       "      <td>Taufeeq Pasha</td>\n",
       "      <td>6300.52</td>\n",
       "      <td>2021-10-01</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>58cf84d3-5fa3-4544-afea-d0a3202563f5</td>\n",
       "      <td>ANOOP PK</td>\n",
       "      <td>100.00</td>\n",
       "      <td>2021-10-02</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>745769ac-5bae-4db6-8d0d-d699497a1a25</td>\n",
       "      <td>SUSHIL RADHESHYAM GIRI</td>\n",
       "      <td>9300.00</td>\n",
       "      <td>2021-10-09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>182283ff-c9a6-47fb-85fa-6959791650e7</td>\n",
       "      <td>IMRAN AHMED M F</td>\n",
       "      <td>7603.48</td>\n",
       "      <td>2021-10-01</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>85b785f9-d649-4027-918b-ab71862e69db</td>\n",
       "      <td>Bejawada Nookaraju</td>\n",
       "      <td>5056.26</td>\n",
       "      <td>2021-10-02</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>...</th>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>134</th>\n",
       "      <td>0b1a6ce6-bd6d-4b0f-b597-9e1b24d69d12</td>\n",
       "      <td>RUPESH SHETYE</td>\n",
       "      <td>1000.00</td>\n",
       "      <td>2021-10-03</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>135</th>\n",
       "      <td>96668b23-52d2-4727-9d72-fb3948b09bff</td>\n",
       "      <td>SAJJAD KHAN</td>\n",
       "      <td>2200.00</td>\n",
       "      <td>2021-10-01</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>136</th>\n",
       "      <td>99466234-78a4-4c1c-8e72-3350616ce138</td>\n",
       "      <td>SHAIKH SARWAR SHAIKH MABUD</td>\n",
       "      <td>5851.50</td>\n",
       "      <td>2021-10-01</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>137</th>\n",
       "      <td>7e749ca9-58e0-4ad5-9be4-b6effd71d3f2</td>\n",
       "      <td>Shayitha Begum</td>\n",
       "      <td>6400.00</td>\n",
       "      <td>2021-10-08</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>138</th>\n",
       "      <td>4f86308b-e53c-41ad-9c84-f8bc74e0007a</td>\n",
       "      <td>Ninad Ingle</td>\n",
       "      <td>500.00</td>\n",
       "      <td>2021-10-03</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>139 rows × 4 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "                                  user_id                   full_name  \\\n",
       "0    61d3dc55-3321-4a65-b6b4-b415d0202030               Taufeeq Pasha   \n",
       "1    58cf84d3-5fa3-4544-afea-d0a3202563f5                    ANOOP PK   \n",
       "2    745769ac-5bae-4db6-8d0d-d699497a1a25      SUSHIL RADHESHYAM GIRI   \n",
       "3    182283ff-c9a6-47fb-85fa-6959791650e7             IMRAN AHMED M F   \n",
       "4    85b785f9-d649-4027-918b-ab71862e69db          Bejawada Nookaraju   \n",
       "..                                    ...                         ...   \n",
       "134  0b1a6ce6-bd6d-4b0f-b597-9e1b24d69d12               RUPESH SHETYE   \n",
       "135  96668b23-52d2-4727-9d72-fb3948b09bff                 SAJJAD KHAN   \n",
       "136  99466234-78a4-4c1c-8e72-3350616ce138  SHAIKH SARWAR SHAIKH MABUD   \n",
       "137  7e749ca9-58e0-4ad5-9be4-b6effd71d3f2              Shayitha Begum   \n",
       "138  4f86308b-e53c-41ad-9c84-f8bc74e0007a                 Ninad Ingle   \n",
       "\n",
       "     repayment   Due date  \n",
       "0      6300.52 2021-10-01  \n",
       "1       100.00 2021-10-02  \n",
       "2      9300.00 2021-10-09  \n",
       "3      7603.48 2021-10-01  \n",
       "4      5056.26 2021-10-02  \n",
       "..         ...        ...  \n",
       "134    1000.00 2021-10-03  \n",
       "135    2200.00 2021-10-01  \n",
       "136    5851.50 2021-10-01  \n",
       "137    6400.00 2021-10-08  \n",
       "138     500.00 2021-10-03  \n",
       "\n",
       "[139 rows x 4 columns]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "total_refunds_2_oct"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "7097f976",
   "metadata": {},
   "outputs": [],
   "source": [
    "def group_clean(df, date):\n",
    "    df1 = df[[\"user_id\", \"repayment\"]]\n",
    "    df1 = df1.groupby(\"user_id\").sum().reset_index()\n",
    "    df2 = df[[\"user_id\", \"Due date\"]]\n",
    "    df2 = df2.groupby(\"user_id\").last().reset_index()\n",
    "    df = pd.merge(df1,df2, on = \"user_id\")\n",
    "    df[\"disbursal(txn) date\"] = df[\"Due date\"]\n",
    "    df.drop([\"Due date\"],1,inplace = True)\n",
    "    df[\"disbursal(txn) date\"] = date\n",
    "    return df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "483e179d",
   "metadata": {},
   "outputs": [],
   "source": [
    "sep_repayment = group_clean(total_refunds_2_oct, \"2021-09-30\")\n",
    "oct_repayment = group_clean(total_refunds_2_nov, \"2021-10-31\")\n",
    "dec_repayment = group_clean(total_refunds_2_dec, \"2021-11-30\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "06c92656",
   "metadata": {},
   "outputs": [],
   "source": [
    "tr = pd.concat([sep_repayment, oct_repayment, dec_repayment])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "3bad9c46",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1114"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tr[\"user_id\"].nunique()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4cc7eb04",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71792867",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "6eb4ca6c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "starting run\n",
      "PostgreSQL server information\n",
      "{'user': 'rainadmin', 'channel_binding': 'prefer', 'dbname': 'rain', 'host': 'localhost', 'port': '55432', 'tty': '', 'options': '', 'sslmode': 'prefer', 'sslcompression': '0', 'ssl_min_protocol_version': 'TLSv1.2', 'gssencmode': 'prefer', 'krbsrvname': 'postgres', 'target_session_attrs': 'any'} \n",
      "\n",
      "You are connected to -  ('PostgreSQL 12.7 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 7.3.1 20180712 (Red Hat 7.3.1-12), 64-bit',) \n",
      "\n"
     ]
    }
   ],
   "source": [
    "print (\"starting run\")\n",
    "session = boto3.session.Session(profile_name=\"rain-india-production\")\n",
    "client = session.client(\"dynamodb\")\n",
    "dynamodb = boto3.resource(\"dynamodb\")\n",
    "\n",
    "connection = psycopg2.connect(user=\"rainadmin\",\n",
    "                                      password=\"Mudar123\",\n",
    "                                      host=\"localhost\",\n",
    "                                      port=55432,\n",
    "                                      database=\"rain\")\n",
    "cursor = connection.cursor()\n",
    "# Print PostgreSQL details\n",
    "print(\"PostgreSQL server information\")\n",
    "print(connection.get_dsn_parameters(), \"\\n\")\n",
    "\n",
    "cursor.execute(\"SELECT version();\")\n",
    "    # Fetch result\n",
    "record = cursor.fetchone()\n",
    "print(\"You are connected to - \", record, \"\\n\")\n",
    "def dataframe_generator(query):\n",
    "    cursor.execute(query)\n",
    "    print('Read table in PostgreSQL')\n",
    "    data = cursor.fetchall()\n",
    "    cols = []\n",
    "    for elt in cursor.description:\n",
    "        cols.append(elt[0])\n",
    "    df= pd.DataFrame(data = data, columns=cols)\n",
    "    return df\n",
    "\n",
    "def clean(df):\n",
    "    df[\"created_at\"] = df[\"created_at\"].dt.date.astype(str)\n",
    "    df = df[df[\"created_at\"]>\"2021-08-31\"]\n",
    "    return df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "3b97c3b9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code'"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "os.getcwd()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "88714c59",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir(\"..\")\n",
    "os.chdir(\"Outputs\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "460517b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_rows_1 = pd.read_csv(\"all_rows.csv\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "ee1a23a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# all_rows_1[\"email\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "dff118b0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from xorg.employers e;\"\"\"\n",
    "xorg = dataframe_generator(query)\n",
    "xorg = xorg[[\"id\",\"organization_id\", \"lookup_name\"]]\n",
    "xorg[\"lookup_name\"] = xorg[\"lookup_name\"].str.lower()\n",
    "xorg.rename(columns={\"id\":\"employer_id\"},inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "9d19be44",
   "metadata": {},
   "outputs": [],
   "source": [
    "xorg = xorg[[\"employer_id\", \"lookup_name\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "0beac430",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(13238, 37)"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "all_rows_1.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "236a326b",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_rows_1 = pd.merge(all_rows_1, xorg, on = \"employer_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "f72fd844",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Index(['Unnamed: 0', 'user_id', 'employee_id', 'full_name', 'birth_date',\n",
       "       'Gender', 'employer_id', 'email', 'status', 'phone_number',\n",
       "       'organization_id', 'document_number', 'created_at', 'paused',\n",
       "       'monthly_salary', 'loan_agreement_number', 'path', 'accepted',\n",
       "       'accepted_at', 'loan_duration', 'loan_Closure_date', 'tid',\n",
       "       'Withdrawn Amount', 'disbursal(txn) date', 'Total Fees', 'Total Amount',\n",
       "       'Loan_Number', 'Total Fees Calculated', 'processing_fees', 'GST_fees',\n",
       "       'Annual_income', 'overall_limit', 'next_payment_date',\n",
       "       'Sanctioned Loan Limit', 'disbursed amount', 'Undisbursed amount',\n",
       "       'Due_Date', 'lookup_name'],\n",
       "      dtype='object')"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "all_rows_1.columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0715cc15",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e96400e",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "1e013b7a",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_rows_1[\"disbursal(txn) date\"] = pd.to_datetime(all_rows_1[\"disbursal(txn) date\"]).dt.date.astype(str)\n",
    "all_rows_1= all_rows_1[['user_id','birth_date', \"email\", 'Gender','organization_id','monthly_salary',\n",
    "           'tid','disbursal(txn) date','Total Amount', 'processing_fees', \n",
    "       'overall_limit', 'Sanctioned Loan Limit', \"lookup_name\"]]\n",
    "all_rows_1[\"organization_id\"].replace({\"c2a6a007-e625-456f-8c36-92cd2654c971\": 'Quess',\n",
    "                                              \"916227f6-cb69-46ec-8cb1-a735ed98f2c4\": 'D2C Org', \n",
    "                                             \"d779558a-09cc-4920-9f39-d8409c8f0728\":\"B2B Test\"},inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "1f237185",
   "metadata": {},
   "outputs": [],
   "source": [
    "gender_dict = {'Male':\"male\", 'MALE':\"male\", 'male':\"male\", 'Female':\"female\", 'M':\"male\", ' MALE ':\"male\", \n",
    "               'F':\"female\", 'female':\"female\"}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "3d0c5a9c",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_rows_1[\"Gender\"] = all_rows_1[\"Gender\"].map(gender_dict)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "00bc04d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_rows_1[\"age\"] = (pd.to_datetime(\"today\") - pd.to_datetime(all_rows_1[\"birth_date\"])).dt.days"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "d56c35e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_rows_1[\"age\"] = round(all_rows_1[\"age\"]/365,0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "ba69bea8",
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = [0, 20, 25, 30, 35, 40, 50, 60,100]\n",
    "labels = [\"under 20\",\"20-25\",\"25-30\",\"30-35\",\"35-40\",\"40-50\", \"50-60\", \"60+\"]\n",
    "all_rows_1['binned_age'] = pd.cut(all_rows_1['age'], bins=bins, labels=labels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "d1329ff5",
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = [0, 15000, 30000, 50000, 80000, 100000, 500000]\n",
    "labels = [\"under 15k\",\"15k-30k\",\"30k-50k\",\"50k-80k\",\"80k-100k\",\"100k+\"]\n",
    "all_rows_1['binned_salary'] = pd.cut(all_rows_1['monthly_salary'], bins=bins, labels=labels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "32396122",
   "metadata": {},
   "outputs": [],
   "source": [
    "# all_rows_1[\"organization_id\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "c4eb583e",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir(\"..\")\n",
    "os.chdir(\"Code\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "161b1dda",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1623, 3)"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tr.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "id": "bfaede2d",
   "metadata": {},
   "outputs": [],
   "source": [
    "tr = pd.merge(tr, all_rows_1[[\"user_id\", \"organization_id\"]].groupby(\"user_id\").last().reset_index(), on = \"user_id\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "id": "85899a9f",
   "metadata": {},
   "outputs": [],
   "source": [
    "scope = [\"https://www.googleapis.com/auth/spreadsheets\", \"https://www.googleapis.com/auth/drive.file\", \n",
    "            \"https://www.googleapis.com/auth/drive\"]\n",
    "creds = ServiceAccountCredentials.from_json_keyfile_name(\"cs-and-ops-dashboard-8febbecf58a8.json\", scope)\n",
    "client_2 = gspread.authorize(creds)\n",
    "employees_kyc_demographic= client_2.open(\"Data Studio Final Dashboard\").worksheet(\"Sheet1\")\n",
    "employees_kyc_demographic.clear()\n",
    "existing = gd.get_as_dataframe(employees_kyc_demographic)\n",
    "existing = pd.DataFrame(employees_kyc_demographic.get_all_records())\n",
    "updated = existing.append(all_rows_1.copy())\n",
    "gd.set_with_dataframe(employees_kyc_demographic, updated)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "id": "9abf8c01",
   "metadata": {},
   "outputs": [],
   "source": [
    "scope = [\"https://www.googleapis.com/auth/spreadsheets\", \"https://www.googleapis.com/auth/drive.file\", \n",
    "            \"https://www.googleapis.com/auth/drive\"]\n",
    "creds = ServiceAccountCredentials.from_json_keyfile_name(\"cs-and-ops-dashboard-8febbecf58a8.json\", scope)\n",
    "client_2 = gspread.authorize(creds)\n",
    "employees_kyc_demographic= client_2.open(\"Data Studio Final Dashboard\").worksheet(\"Sheet2\")\n",
    "employees_kyc_demographic.clear()\n",
    "existing = gd.get_as_dataframe(employees_kyc_demographic)\n",
    "existing = pd.DataFrame(employees_kyc_demographic.get_all_records())\n",
    "updated = existing.append(tr.copy())\n",
    "gd.set_with_dataframe(employees_kyc_demographic, updated)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93a9f81d",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "id": "3320d158",
   "metadata": {},
   "outputs": [],
   "source": [
    "# iam[\"status\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "id": "8314aba9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from iam.users u ;\"\"\"\n",
    "iam = dataframe_generator(query)\n",
    "iam = clean(iam)\n",
    "iam.rename(columns={\"id\":\"user_id\"},inplace=True)\n",
    "phone_number = iam[\"phone_number\"].astype(str).tolist()\n",
    "phone_numbers = []\n",
    "for x in phone_number:\n",
    "    phone_numbers.append(re.sub(\"[^0-9]\", \"\", x))\n",
    "phone_number_2 =[]\n",
    "for x in phone_numbers:\n",
    "    if len(x)>10:\n",
    "        phone_number_2.append(x[2:])\n",
    "    else:\n",
    "        phone_number_2.append(x)\n",
    "iam[\"phone_number\"] = phone_number_2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "686df97f",
   "metadata": {},
   "outputs": [],
   "source": [
    "iam = iam[[\"user_id\", \"full_name\", \"status\", \"email\", \"phone_number\", \"created_at\", \"metadata\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "id": "fb605d4f",
   "metadata": {},
   "outputs": [],
   "source": [
    "gender=[]\n",
    "birth_date=[]\n",
    "for i in range(0,iam.shape[0]):\n",
    "    gender.append(iam[\"metadata\"].iloc[i][\"gender\"])\n",
    "    birth_date.append(iam[\"metadata\"].iloc[i][\"birth_date\"])\n",
    "\n",
    "iam[\"Gender\"] = gender\n",
    "iam[\"birth_date\"] = birth_date\n",
    "iam.drop([\"metadata\"],1,inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "53a9a73b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from ems.employees e ;\"\"\"\n",
    "ems_employees = dataframe_generator(query)\n",
    "ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',\n",
    "        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]\n",
    "ems_employees = ems_employees.rename(columns={'id': 'employee_id'})\n",
    "ems_employees= clean(ems_employees)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "id": "22c2ae4a",
   "metadata": {},
   "outputs": [],
   "source": [
    "ems_employees = ems_employees[[\"employee_id\", \"user_id\", \"employer_id\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "id": "141a5821",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from xorg.employers e;\"\"\"\n",
    "xorg = dataframe_generator(query)\n",
    "xorg = xorg[[\"id\",\"organization_id\", \"lookup_name\"]]\n",
    "xorg[\"lookup_name\"] = xorg[\"lookup_name\"].str.lower()\n",
    "xorg.rename(columns={\"id\":\"employer_id\"},inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "id": "4e51f7eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "xorg[\"organization_id\"].replace({\"c2a6a007-e625-456f-8c36-92cd2654c971\": 'Quess',\n",
    "                                              \"916227f6-cb69-46ec-8cb1-a735ed98f2c4\": 'D2C Org', \n",
    "                                             \"d779558a-09cc-4920-9f39-d8409c8f0728\":\"B2B Test\"},inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "id": "d1521263",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(14199, 3)"
      ]
     },
     "execution_count": 46,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ems_employees.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "id": "131e6c0c",
   "metadata": {},
   "outputs": [],
   "source": [
    "ems_xorg= pd.merge(ems_employees, xorg, on = \"employer_id\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "id": "3f284462",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(14199, 5)"
      ]
     },
     "execution_count": 48,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ems_xorg.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "id": "77f47438",
   "metadata": {},
   "outputs": [],
   "source": [
    "iam_ems_xorg = pd.merge(iam, ems_xorg, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "id": "bea12b62",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(20192, 12)"
      ]
     },
     "execution_count": 50,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "iam_ems_xorg.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "id": "77d39303",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Quess       9161\n",
       "D2C Org     4465\n",
       "B2B Test       2\n",
       "Name: organization_id, dtype: int64"
      ]
     },
     "execution_count": 51,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "iam_ems_xorg[\"organization_id\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "id": "b358cd08",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ACTIVE                  18978\n",
       "CONFIRMATION_PENDING     1214\n",
       "Name: status, dtype: int64"
      ]
     },
     "execution_count": 52,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "iam_ems_xorg[\"status\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "id": "cd982a13",
   "metadata": {},
   "outputs": [],
   "source": [
    "quess = iam_ems_xorg[iam_ems_xorg[\"organization_id\"]==\"Quess\"]\n",
    "d2c = iam_ems_xorg[iam_ems_xorg[\"organization_id\"]==\"D2C Org\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "id": "0acc99bd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from risk.user_risk_verifications urv; \"\"\"\n",
    "cv = dataframe_generator(query)\n",
    "cv = clean(cv)\n",
    "\n",
    "cv.rename(columns={\"score\":\"Approved\"},inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "id": "4819aae8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>user_id</th>\n",
       "      <th>employment</th>\n",
       "      <th>Approved</th>\n",
       "      <th>zip_code</th>\n",
       "      <th>created_at</th>\n",
       "      <th>updated_at</th>\n",
       "      <th>underwriting</th>\n",
       "      <th>fraud</th>\n",
       "      <th>kyc</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>02228250-afd1-4490-be7e-fdd472ff5adc</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>True</td>\n",
       "      <td>2021-11-11</td>\n",
       "      <td>2021-11-11 13:25:22.118661+00:00</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>24d8a8ae-e311-4499-9137-765c8d44acfe</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>True</td>\n",
       "      <td>2021-11-11</td>\n",
       "      <td>2021-11-11 13:26:26.605396+00:00</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>a03b4a4a-8b63-4b36-964b-778b027772d4</td>\n",
       "      <td>None</td>\n",
       "      <td>True</td>\n",
       "      <td>True</td>\n",
       "      <td>2021-11-11</td>\n",
       "      <td>2021-11-11 13:28:58.122928+00:00</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>0869f258-e9f7-4baa-84ea-b4d85393a9d9</td>\n",
       "      <td>True</td>\n",
       "      <td>True</td>\n",
       "      <td>True</td>\n",
       "      <td>2021-09-08</td>\n",
       "      <td>2021-11-03 02:24:18.497073+00:00</td>\n",
       "      <td>False</td>\n",
       "      <td>False</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>92cbc9a6-aef0-4a40-96d5-5741cadf32bf</td>\n",
       "      <td>None</td>\n",
       "      <td>True</td>\n",
       "      <td>True</td>\n",
       "      <td>2021-09-01</td>\n",
       "      <td>2021-09-01 11:24:27.761503+00:00</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                                 user_id employment Approved zip_code  \\\n",
       "3   02228250-afd1-4490-be7e-fdd472ff5adc       None     None     True   \n",
       "4   24d8a8ae-e311-4499-9137-765c8d44acfe       None     None     True   \n",
       "7   a03b4a4a-8b63-4b36-964b-778b027772d4       None     True     True   \n",
       "9   0869f258-e9f7-4baa-84ea-b4d85393a9d9       True     True     True   \n",
       "10  92cbc9a6-aef0-4a40-96d5-5741cadf32bf       None     True     True   \n",
       "\n",
       "    created_at                       updated_at underwriting  fraud   kyc  \n",
       "3   2021-11-11 2021-11-11 13:25:22.118661+00:00         None   None  None  \n",
       "4   2021-11-11 2021-11-11 13:26:26.605396+00:00         None   None  None  \n",
       "7   2021-11-11 2021-11-11 13:28:58.122928+00:00         None   None  None  \n",
       "9   2021-09-08 2021-11-03 02:24:18.497073+00:00        False  False  None  \n",
       "10  2021-09-01 2021-09-01 11:24:27.761503+00:00         None   None  None  "
      ]
     },
     "execution_count": 55,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cv.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "id": "ccfd1030",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir(\"..\")\n",
    "os.chdir(\"AWS_Data/\")\n",
    "rootdir = os.getcwd()\n",
    "files_dump =[]\n",
    "for subdir, dirs, files in os.walk(rootdir):\n",
    "    for file in files:\n",
    "#             print(os.path.join(subdir, file))\n",
    "        if file.endswith(\"json\"):\n",
    "            files_dump.append(os.path.join(subdir, file))\n",
    "ff = []\n",
    "for x in files_dump:\n",
    "    fff = {}\n",
    "    f = open(x)\n",
    "    try:\n",
    "        data = json.load(f)\n",
    "    except:\n",
    "        data = \"Json Failure - Issue at our AWS end\"\n",
    "    fff[\"user_id\"] = str(f).split(\"/\")[-2]\n",
    "    fff[\"data\"] = data\n",
    "    ff.append(fff)\n",
    "ffff = pd.DataFrame(ff)\n",
    "os.chdir(\"..\")\n",
    "os.chdir(\"Code\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "id": "411e867b",
   "metadata": {},
   "outputs": [],
   "source": [
    "aws_approved = []\n",
    "for x in ffff[\"data\"]:\n",
    "    if x==\"Yes\":\n",
    "        aws_approved.append(True)\n",
    "    else:\n",
    "        aws_approved.append(False)\n",
    "ffff[\"Aws Approved\"] = aws_approved\n",
    "cv = pd.merge(cv,ffff, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "id": "279428c1",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True     9081\n",
       "False    2890\n",
       "Name: Approved, dtype: int64"
      ]
     },
     "execution_count": 58,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cv[\"Approved\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5994025c",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "id": "f6f4e50b",
   "metadata": {},
   "outputs": [],
   "source": [
    "quess_cv = cv[[\"user_id\", \"Approved\", \"data\"]]\n",
    "d2c_cv = cv[[\"user_id\", \"Approved\", \"data\", \"underwriting\", \"fraud\", \"kyc\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "id": "5941b66b",
   "metadata": {},
   "outputs": [],
   "source": [
    "quess = pd.merge(quess, quess_cv, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "id": "0d8f9558",
   "metadata": {},
   "outputs": [],
   "source": [
    "quess[\"Approved\"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "359f17fe",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "554071e3",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "id": "76b318a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c = pd.merge(d2c, d2c_cv, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "id": "22e72c08",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c[\"Approved\"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "id": "db5edcdc",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c['fraud'] = d2c['fraud'].replace({False: 'Rejected', True: 'Approved'})\n",
    "d2c['underwriting'] = d2c['underwriting'].replace({False: 'Rejected', True: 'Approved'})\n",
    "d2c[\"kyc\"] = d2c[\"kyc\"].replace({False: 'Rejected', True: 'Approved'})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "id": "71daca87",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Approved    2256\n",
       "Rejected     173\n",
       "Name: fraud, dtype: int64"
      ]
     },
     "execution_count": 65,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d2c[\"fraud\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "id": "baf90899",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from elog.events e;\"\"\"\n",
    "elog = dataframe_generator(query)\n",
    "categories = elog[\"action\"].value_counts().index.tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 67,
   "id": "58a19355",
   "metadata": {},
   "outputs": [],
   "source": [
    "# elog"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 68,
   "id": "8d335ce5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from kyc.documents kyc ;\"\"\"\n",
    "kyc = dataframe_generator(query)\n",
    "kyc = clean(kyc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 69,
   "id": "15518ae0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# kyc.columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 70,
   "id": "19c4d177",
   "metadata": {},
   "outputs": [],
   "source": [
    "kyc = kyc.groupby(\"user_id\").last().reset_index()[[\"user_id\", \"approved\", \"document_type\", \"side\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 71,
   "id": "4673e311",
   "metadata": {},
   "outputs": [],
   "source": [
    "kyc.rename(columns={\"approved\":\"kyc_approved\"},inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 72,
   "id": "a7662ee0",
   "metadata": {},
   "outputs": [],
   "source": [
    "quess = pd.merge(quess, kyc, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 73,
   "id": "0ebc3cf2",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c = pd.merge(d2c, kyc, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "id": "cc153515",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c[\"kyc_approved\"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "id": "7f38e757",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Approved    168\n",
       "Rejected     28\n",
       "Name: kyc_approved, dtype: int64"
      ]
     },
     "execution_count": 75,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d2c[\"kyc_approved\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "id": "1592cb2d",
   "metadata": {},
   "outputs": [],
   "source": [
    "quess[\"kyc_approved\"].replace({False: 'Rejected', True: 'Approved'}, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 77,
   "id": "ed05ddef",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Approved    2537\n",
       "Rejected     273\n",
       "Name: kyc_approved, dtype: int64"
      ]
     },
     "execution_count": 77,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "quess[\"kyc_approved\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 78,
   "id": "6f1be86c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# all_rows_1.groupby([\"disbursal(txn) date\", \"user_id\"]).sum().reset_index()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 79,
   "id": "96dba83b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from bnk.external_accounts ea ;\"\"\"\n",
    "bnk_external = dataframe_generator(query)\n",
    "bnk_external = clean(bnk_external)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 80,
   "id": "e7668411",
   "metadata": {},
   "outputs": [],
   "source": [
    "# bnk_external.columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 81,
   "id": "47bdbdf0",
   "metadata": {},
   "outputs": [],
   "source": [
    "bnk_external = bnk_external[[\"user_id\", \"status\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 82,
   "id": "0c99b39f",
   "metadata": {},
   "outputs": [],
   "source": [
    "bnk_external.rename(columns={\"status\":\"bank_status\"},inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 83,
   "id": "378506dd",
   "metadata": {},
   "outputs": [],
   "source": [
    "bnk_external = bnk_external[bnk_external[\"bank_status\"]==\"ACTIVE\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 84,
   "id": "1ca8103d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(9161, 17)"
      ]
     },
     "execution_count": 84,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "quess.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "id": "68569d45",
   "metadata": {},
   "outputs": [],
   "source": [
    "quess = pd.merge(quess, bnk_external, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 86,
   "id": "ee4a30e2",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ACTIVE    1597\n",
       "Name: bank_status, dtype: int64"
      ]
     },
     "execution_count": 86,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "quess[\"bank_status\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 87,
   "id": "1ff73247",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c = pd.merge(d2c,bnk_external, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 88,
   "id": "58f2e020",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Read table in PostgreSQL\n"
     ]
    }
   ],
   "source": [
    "query = \"\"\"select * from ems.loan_agreements la ;\"\"\"\n",
    "la = dataframe_generator(query)\n",
    "la = clean(la)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 89,
   "id": "f36dac87",
   "metadata": {},
   "outputs": [],
   "source": [
    "# la.columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 90,
   "id": "82a4637d",
   "metadata": {},
   "outputs": [],
   "source": [
    "la = la[[\"employee_id\", \"accepted\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 91,
   "id": "5bac9caa",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True     1671\n",
       "False       6\n",
       "Name: accepted, dtype: int64"
      ]
     },
     "execution_count": 91,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "la[\"accepted\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 92,
   "id": "5683eb78",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(9161, 18)"
      ]
     },
     "execution_count": 92,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "quess.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 93,
   "id": "6274071a",
   "metadata": {},
   "outputs": [],
   "source": [
    "quess = pd.merge(quess, la, on = \"employee_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 94,
   "id": "dd13dedd",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c = pd.merge(d2c, la, on = \"employee_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 95,
   "id": "12b7dac7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ACTIVE    78\n",
       "Name: bank_status, dtype: int64"
      ]
     },
     "execution_count": 95,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d2c[\"bank_status\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 96,
   "id": "94f4b492",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True    73\n",
       "Name: accepted, dtype: int64"
      ]
     },
     "execution_count": 96,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d2c[\"accepted\"].value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 97,
   "id": "8a54f4cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "scope = [\"https://www.googleapis.com/auth/spreadsheets\", \"https://www.googleapis.com/auth/drive.file\", \n",
    "            \"https://www.googleapis.com/auth/drive\"]\n",
    "creds = ServiceAccountCredentials.from_json_keyfile_name(\"cs-and-ops-dashboard-8febbecf58a8.json\", scope)\n",
    "client_2 = gspread.authorize(creds)\n",
    "employees_kyc_demographic= client_2.open(\"Data Studio Final Dashboard\").worksheet(\"Sheet4\")\n",
    "employees_kyc_demographic.clear()\n",
    "existing = gd.get_as_dataframe(employees_kyc_demographic)\n",
    "existing = pd.DataFrame(employees_kyc_demographic.get_all_records())\n",
    "updated = existing.append(quess.copy())\n",
    "gd.set_with_dataframe(employees_kyc_demographic, updated)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 98,
   "id": "98c48851",
   "metadata": {},
   "outputs": [],
   "source": [
    "scope = [\"https://www.googleapis.com/auth/spreadsheets\", \"https://www.googleapis.com/auth/drive.file\", \n",
    "            \"https://www.googleapis.com/auth/drive\"]\n",
    "creds = ServiceAccountCredentials.from_json_keyfile_name(\"cs-and-ops-dashboard-8febbecf58a8.json\", scope)\n",
    "client_2 = gspread.authorize(creds)\n",
    "employees_kyc_demographic= client_2.open(\"Data Studio Final Dashboard\").worksheet(\"Sheet5\")\n",
    "employees_kyc_demographic.clear()\n",
    "existing = gd.get_as_dataframe(employees_kyc_demographic)\n",
    "existing = pd.DataFrame(employees_kyc_demographic.get_all_records())\n",
    "updated = existing.append(d2c.copy())\n",
    "gd.set_with_dataframe(employees_kyc_demographic, updated)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6fd20cc3",
   "metadata": {},
   "source": [
    "### D2C Metrics Update"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5fdfc26d",
   "metadata": {},
   "outputs": [],
   "source": [
    "query = \"\"\"select * from iam.users u ;\"\"\"\n",
    "iam = dataframe_generator(query)\n",
    "iam = clean(iam)\n",
    "iam.rename(columns={\"id\":\"user_id\"},inplace=True)\n",
    "phone_number = iam[\"phone_number\"].astype(str).tolist()\n",
    "phone_numbers = []\n",
    "for x in phone_number:\n",
    "    phone_numbers.append(re.sub(\"[^0-9]\", \"\", x))\n",
    "phone_number_2 =[]\n",
    "for x in phone_numbers:\n",
    "    if len(x)>10:\n",
    "        phone_number_2.append(x[2:])\n",
    "    else:\n",
    "        phone_number_2.append(x)\n",
    "iam[\"phone_number\"] = phone_number_2\n",
    "query = \"\"\"select * from ems.employees e ;\"\"\"\n",
    "ems_employees = dataframe_generator(query)\n",
    "ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',\n",
    "        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]\n",
    "ems_employees = ems_employees.rename(columns={'id': 'employee_id'})\n",
    "ems_employees= clean(ems_employees)\n",
    "query = \"\"\"select * from xorg.employers e;\"\"\"\n",
    "xorg = dataframe_generator(query)\n",
    "xorg = xorg[[\"id\", \"lookup_name\"]]\n",
    "xorg[\"lookup_name\"] = xorg[\"lookup_name\"].str.lower()\n",
    "xorg.rename(columns={\"id\":\"employer_id\"},inplace=True)\n",
    "ems_xorg = pd.merge(ems_employees, xorg, on = \"employer_id\")\n",
    "iam_users = iam[[\"user_id\", \"full_name\", \"email\",\"phone_number\", \"metadata\",\"document_number\"]]\n",
    "iam_ems_employees = pd.merge(ems_xorg, iam_users, on = \"user_id\")\n",
    "iam_ems_employees = iam_ems_employees.sort_values(\"created_at\")\n",
    "gender=[]\n",
    "birth_date=[]\n",
    "for i in range(0,iam_ems_employees.shape[0]):\n",
    "    gender.append(iam_ems_employees[\"metadata\"].iloc[i][\"gender\"])\n",
    "    birth_date.append(iam_ems_employees[\"metadata\"].iloc[i][\"birth_date\"])\n",
    "\n",
    "iam_ems_employees[\"Gender\"] = gender\n",
    "iam_ems_employees[\"birth_date\"] = birth_date\n",
    "iam_ems_employees.drop([\"metadata\"],1,inplace=True)\n",
    "iam_ems_employees = iam_ems_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',\n",
    "'organization_id','document_number','created_at', \"lookup_name\"]]\n",
    "d2c = iam_ems_employees[iam_ems_employees[\"organization_id\"]==\"916227f6-cb69-46ec-8cb1-a735ed98f2c4\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f907f60f",
   "metadata": {},
   "outputs": [],
   "source": [
    "uid = d2c[\"user_id\"].unique().tolist()\n",
    "query = \"\"\"select * from risk.user_risk_verifications urv; \"\"\"\n",
    "cv = dataframe_generator(query)\n",
    "cv = clean(cv)\n",
    "cv.rename(columns={\"score\":\"Bureau Approved\"\n",
    "                  },inplace=True)\n",
    "cv = cv[cv[\"user_id\"].isin(uid)]\n",
    "cv = cv[['user_id','Bureau Approved','underwriting', 'fraud', 'kyc']]\n",
    "cv = cv.fillna(\"API Not Hit/No Value\")\n",
    "cv['Bureau Approved'] = cv['Bureau Approved'].replace({False: 'Rejected', True: 'Approved'})\n",
    "cv['fraud'] = cv['fraud'].replace({False: 'Rejected', True: 'Approved'})\n",
    "cv['underwriting'] = cv['underwriting'].replace({False: 'Rejected', True: 'Approved'})\n",
    "cv[\"kyc\"] = cv[\"kyc\"].replace({False: 'Rejected', True: 'Approved'})\n",
    "d2c = pd.merge(d2c,cv,on = \"user_id\", how = \"left\")\n",
    "d2c[\"Bureau Approved\"] = d2c[\"Bureau Approved\"].fillna(\"API Not Hit/No Value\")\n",
    "d2c[\"fraud\"] = d2c[\"fraud\"].fillna(\"API Not Hit/No Value\")\n",
    "d2c[\"underwriting\"] = d2c[\"underwriting\"].fillna(\"API Not Hit/No Value\")\n",
    "d2c[\"kyc\"] = d2c[\"kyc\"].fillna(\"API Not Hit/No Value\")\n",
    "query = \"\"\"select * from elog.events e;\"\"\"\n",
    "elog = dataframe_generator(query)\n",
    "categories = elog[\"action\"].value_counts().index.tolist()\n",
    "risk = []\n",
    "for x in categories:\n",
    "    if x.startswith(\"risk\"):\n",
    "        print (x)\n",
    "        risk.append(x)\n",
    "all_d = []\n",
    "for x in risk:\n",
    "    d = elog[elog[\"action\"]==x]\n",
    "    u = []\n",
    "    a = []\n",
    "    r = []\n",
    "    for x in d[\"body\"].tolist():\n",
    "        u.append(x[\"userID\"])\n",
    "        a.append(x[\"action\"])\n",
    "        try:\n",
    "            r.append(x[\"reason\"])\n",
    "        except:\n",
    "            r.append(\"\")\n",
    "    d1 = pd.DataFrame(u, columns=[\"user_id\"])\n",
    "    d1[\"action\"] = a[0]\n",
    "    d1[\"reason\"] = r\n",
    "    all_d.append(d1)\n",
    "all_d = pd.concat(all_d)\n",
    "d2c_phone = d2c[d2c[\"user_id\"].isin(all_d[\"user_id\"].unique().tolist())][[\"user_id\",\"phone_number\" ]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d6d7e52",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_d_phone = pd.merge(all_d,d2c_phone, on = \"user_id\", how = \"left\").fillna(\"No Phone number\")\n",
    "def phone_number_checker(phone_number):\n",
    "    phone_number = str(phone_number)\n",
    "    u = d2c[d2c[\"phone_number\"]==phone_number][\"user_id\"].tolist()[-1]\n",
    "    m = all_d_phone[all_d_phone[\"user_id\"]==u]\n",
    "    n = m[m[\"reason\"]!=\"\"][[\"user_id\", \"action\",\"reason\"]]\n",
    "    if n.shape[0]>0:\n",
    "        d3 = n[\"reason\"].unique().tolist()\n",
    "    else:\n",
    "        d3 = []\n",
    "    return d3\n",
    "print (phone_number_checker(9620126779))\n",
    "ph = d2c[\"phone_number\"].tolist()\n",
    "ph_d = []\n",
    "for x in ph:\n",
    "    m = {}\n",
    "    m[x] = phone_number_checker(x)\n",
    "    ph_d.append(m)\n",
    "ph_d = {k:v for element in ph_d for k,v in element.items()}\n",
    "d2c[\"phone\"] = d2c[\"phone_number\"]\n",
    "d2c[\"phone\"] = d2c[\"phone\"].map(ph_d)\n",
    "d2c.rename(columns={\"phone\":\"Rejection Reason\"},inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d955bf90",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir(\"..\")\n",
    "os.chdir(\"Outputs\")\n",
    "all_rows_1 = pd.read_csv(\"all_rows.csv\")\n",
    "os.chdir(\"..\")\n",
    "os.chdir(\"Code\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c93c6501",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c_rows_1 = all_rows_1[all_rows_1[\"user_id\"].isin(d2c[\"user_id\"].tolist())]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a434a80a",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c_rows_1 = d2c_rows_1[['user_id', 'paused',\n",
    "       'monthly_salary', 'loan_agreement_number', 'tid',\n",
    "       'Withdrawn Amount', 'disbursal(txn) date', 'Total Fees', 'Total Amount',\n",
    "       'Total Fees Calculated', 'processing_fees', 'GST_fees',\n",
    "       'Annual_income', 'overall_limit']]\n",
    "d2c_rows_1[\"disbursal(txn) date\"] = pd.to_datetime(d2c_rows_1[\"disbursal(txn) date\"]).dt.date.astype(str)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35d67b19",
   "metadata": {},
   "outputs": [],
   "source": [
    "d2c = pd.merge(d2c,d2c_rows_1, on = \"user_id\", how = \"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "496da025",
   "metadata": {},
   "outputs": [],
   "source": [
    "query = \"\"\"select * from ems.loan_agreements la ;\"\"\"\n",
    "loan_agreements = dataframe_generator(query)\n",
    "loan_agreements = clean(loan_agreements)\n",
    "loan_agreements=loan_agreements[[\"employee_id\", \"loan_agreement_number\", \"expiration_date\", \"path\", \"accepted\", \"accepted_at\"]]\n",
    "loan_agreements[\"expiration_date\"] = pd.to_datetime(loan_agreements[\"expiration_date\"]).dt.date\n",
    "loan_agreements[\"today\"] = pd.to_datetime(\"today\")\n",
    "loan_agreements[\"today\"] = loan_agreements[\"today\"].dt.date\n",
    "loan_agreements[\"loan_duration\"] = loan_agreements[\"expiration_date\"] - loan_agreements[\"today\"]\n",
    "loan_agreements[\"loan_Closure_date\"] = loan_agreements[\"expiration_date\"]\n",
    "loan_agreements[\"accepted_at\"] = pd.to_datetime(loan_agreements[\"accepted_at\"]).dt.date\n",
    "loan_agreements.drop([\"expiration_date\",\"today\"],1,inplace=True)\n",
    "loan_agreements.rename(columns={\"accepted_date\":\"loan_agreement_date\"},inplace=True)\n",
    "loan_agreements = loan_agreements[[\"employee_id\", \"accepted\"]]\n",
    "loan_agreements.rename(columns={\"accepted\":\"Loan agreement accepted\"},inplace=True)\n",
    "d2c = pd.merge(d2c,loan_agreements, on = \"employee_id\", how =\"left\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7b725eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "scope = [\"https://www.googleapis.com/auth/spreadsheets\", \"https://www.googleapis.com/auth/drive.file\", \n",
    "        \"https://www.googleapis.com/auth/drive\"]\n",
    "creds = ServiceAccountCredentials.from_json_keyfile_name(\"cs-and-ops-dashboard-8febbecf58a8.json\", scope)\n",
    "client_2 = gspread.authorize(creds)\n",
    "employees_kyc_demographic= client_2.open(\"CS/OPS Dashboard\").worksheet(\"D2C_Funnel_Test\")\n",
    "existing = gd.get_as_dataframe(employees_kyc_demographic)\n",
    "existing = pd.DataFrame(employees_kyc_demographic.get_all_records())\n",
    "updated = existing.append(d2c.copy())\n",
    "gd.set_with_dataframe(employees_kyc_demographic, updated)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b51c42f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "scope = [\"https://www.googleapis.com/auth/spreadsheets\", \"https://www.googleapis.com/auth/drive.file\", \n",
    "        \"https://www.googleapis.com/auth/drive\"]\n",
    "creds = ServiceAccountCredentials.from_json_keyfile_name(\"cs-and-ops-dashboard-8febbecf58a8.json\", scope)\n",
    "client_2 = gspread.authorize(creds)\n",
    "employees_kyc_demographic= client_2.open(\"Internal Tracker - Marketing\").worksheet(\"D2C Dump\")\n",
    "employees_kyc_demographic.clear()\n",
    "existing = gd.get_as_dataframe(employees_kyc_demographic)\n",
    "existing = pd.DataFrame(employees_kyc_demographic.get_all_records())\n",
    "updated = existing.append(d2c.copy())\n",
    "gd.set_with_dataframe(employees_kyc_demographic, updated)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af2c818f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# iam_ems_employees[iam_ems_employees[\"phone_number\"]==\"8668873382\"]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
