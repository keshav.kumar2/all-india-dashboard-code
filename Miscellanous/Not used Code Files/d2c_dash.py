#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 16:03:20 2021

@author: arunabhmajumdar
"""

import base64
import boto3
import pandas as pd
import os
import time
import warnings
warnings.filterwarnings("ignore")
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
import numpy as np
import warnings
warnings.filterwarnings("ignore")

import time
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')



connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")

cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df



query = """select * from iam.users u ;"""
iam = dataframe_generator(query)
iam = clean(iam)
iam.rename(columns={"id":"user_id"},inplace=True)
phone_number = iam["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam["phone_number"] = phone_number_2



query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
        'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
ems_employees= clean(ems_employees)



query = """select * from xorg.employers e;"""
xorg = dataframe_generator(query)
xorg = xorg[["id", "lookup_name"]]
xorg["lookup_name"] = xorg["lookup_name"].str.lower()
xorg.rename(columns={"id":"employer_id"},inplace=True)
ems_xorg = pd.merge(ems_employees, xorg, on = "employer_id")




iam_users = iam[["user_id", "full_name", "email","phone_number", "metadata","document_number"]]
iam_ems_employees = pd.merge(ems_xorg, iam_users, on = "user_id")
iam_ems_employees = iam_ems_employees.sort_values("created_at")
gender=[]
birth_date=[]
for i in range(0,iam_ems_employees.shape[0]):
    gender.append(iam_ems_employees["metadata"].iloc[i]["gender"])
    birth_date.append(iam_ems_employees["metadata"].iloc[i]["birth_date"])

iam_ems_employees["Gender"] = gender
iam_ems_employees["birth_date"] = birth_date
iam_ems_employees.drop(["metadata"],1,inplace=True)
iam_ems_employees = iam_ems_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',
'organization_id','document_number','created_at', "lookup_name"]]




d2c = iam_ems_employees[iam_ems_employees["organization_id"]=="916227f6-cb69-46ec-8cb1-a735ed98f2c4"]



uid = d2c["user_id"].unique().tolist()


query = """select * from risk.user_risk_verifications urv; """
cv = dataframe_generator(query)
cv = clean(cv)
cv.rename(columns={"score":"Bureau Approved"
                  },inplace=True)



cv = cv[cv["user_id"].isin(uid)]

cv = cv[['user_id','Bureau Approved','underwriting', 'fraud', 'kyc']]
cv = cv.fillna("API Not Hit/No Value")
cv['Bureau Approved'] = cv['Bureau Approved'].replace({False: 'Rejected', True: 'Approved'})
cv['fraud'] = cv['fraud'].replace({False: 'Rejected', True: 'Approved'})
cv['underwriting'] = cv['underwriting'].replace({False: 'Rejected', True: 'Approved'})

cv["kyc"] = cv["kyc"].replace({False: 'Rejected', True: 'Approved'})

d2c = pd.merge(d2c,cv,on = "user_id", how = "left")
d2c["Bureau Approved"] = d2c["Bureau Approved"].fillna("API Not Hit/No Value")
d2c["fraud"] = d2c["fraud"].fillna("API Not Hit/No Value")
d2c["underwriting"] = d2c["underwriting"].fillna("API Not Hit/No Value")
d2c["kyc"] = d2c["kyc"].fillna("API Not Hit/No Value")

query = """select * from elog.events e;"""
elog = dataframe_generator(query)
categories = elog["action"].value_counts().index.tolist()
risk = []
for x in categories:
    if x.startswith("risk"):
        print (x)
        risk.append(x)
        
all_d = []
for x in risk:
    d = elog[elog["action"]==x]
    u = []
    a = []
    r = []
    for x in d["body"].tolist():
        u.append(x["userID"])
        a.append(x["action"])
        try:
            r.append(x["reason"])
        except:
            r.append("")
    d1 = pd.DataFrame(u, columns=["user_id"])
    d1["action"] = a[0]
    d1["reason"] = r
    all_d.append(d1)
    
    
all_d = pd.concat(all_d)
d2c_phone = d2c[d2c["user_id"].isin(all_d["user_id"].unique().tolist())][["user_id","phone_number" ]]

all_d_phone = pd.merge(all_d,d2c_phone, on = "user_id", how = "left").fillna("No Phone number")


def phone_number_checker(phone_number):
    phone_number = str(phone_number)
    u = d2c[d2c["phone_number"]==phone_number]["user_id"].tolist()[-1]
    m = all_d_phone[all_d_phone["user_id"]==u]
    n = m[m["reason"]!=""][["user_id", "action","reason"]]
    if n.shape[0]>0:
        d3 = n["reason"].unique().tolist()
    else:
        d3 = []
    return d3

print (phone_number_checker(9620126779))

ph = d2c["phone_number"].tolist()
ph_d = []
for x in ph:
    m = {}
    m[x] = phone_number_checker(x)
    ph_d.append(m)
    
ph_d = {k:v for element in ph_d for k,v in element.items()}

d2c["phone"] = d2c["phone_number"]
d2c["phone"] = d2c["phone"].map(ph_d)
d2c.rename(columns={"phone":"Rejection Reason"},inplace=True)
query = """select * from bnk.transactions t  ;"""
txns = dataframe_generator(query)
txns["second_creation_dummy"] = txns["created_at"]
txns = clean(txns)
txns.rename(columns={"entity_id":"user_id"},inplace=True)
txns.rename(columns={"amount":"Withdrawn Amount"}, inplace=True)
txns.rename(columns={"second_creation_dummy":"disbursal(txn) date"},inplace=True)
txns.rename(columns={"fee":"Total Fees"},inplace=True)
# txns.rename(columns={"reference_id":"Loan Number"},inplace=True)
# txns["Loan Number"] = "'"+txns["Loan Number"]
txns["Total Fees"] = txns["Total Fees"]/100
txns = txns[txns["status"]=="COMPLETE"]
txns = txns[["id","user_id", "Withdrawn Amount","disbursal(txn) date", "Total Fees"]]
txns["Withdrawn Amount"] = txns["Withdrawn Amount"]/100
txns.rename(columns={"id":"tid"},inplace=True)
txns["Total Amount"] = txns["Withdrawn Amount"]+txns["Total Fees"]
d2c = pd.merge(d2c, txns[txns["user_id"].isin(uid)], on= "user_id", how = "left")
query = """select * from ems.loan_agreements la ;"""
loan_agreements = dataframe_generator(query)
loan_agreements = clean(loan_agreements)
loan_agreements=loan_agreements[["employee_id", "loan_agreement_number", "expiration_date", "path", "accepted", "accepted_at"]]
loan_agreements["expiration_date"] = pd.to_datetime(loan_agreements["expiration_date"]).dt.date
loan_agreements["today"] = pd.to_datetime("today")
loan_agreements["today"] = loan_agreements["today"].dt.date
loan_agreements["loan_duration"] = loan_agreements["expiration_date"] - loan_agreements["today"]
loan_agreements["loan_Closure_date"] = loan_agreements["expiration_date"]
loan_agreements["accepted_at"] = pd.to_datetime(loan_agreements["accepted_at"]).dt.date
loan_agreements.drop(["expiration_date","today"],1,inplace=True)
loan_agreements.rename(columns={"accepted_date":"loan_agreement_date"},inplace=True)
loan_agreements = loan_agreements[["employee_id", "accepted"]]
loan_agreements.rename(columns={"accepted":"Loan agreement accepted"},inplace=True)

d2c = pd.merge(d2c,loan_agreements, on = "employee_id", how ="left")



scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
        "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("CS/OPS Dashboard").worksheet("D2C_Funnel_Test")
employees_kyc_demographic.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
updated = existing.append(d2c.copy())
gd.set_with_dataframe(employees_kyc_demographic, updated)
