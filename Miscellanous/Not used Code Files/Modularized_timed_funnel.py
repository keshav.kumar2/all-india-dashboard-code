#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 21 11:39:17 2021

@author: arunabhmajumdar
"""

# Importing packages

import pandas as pd,os, warnings
warnings.filterwarnings("ignore")
import base64
import boto3 #Package to connect to AWS Services
import time
# importing packages which connect to Google sheets or Google Data Studio

from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2 #Pacakage to connect to postgres
import df2gspread as d2g
import pandas as pd

#Importing Other packages
import numpy as np
import re
import calendar 
import json

# Changing time zone to India because txns come in Brazilian time. 

import time
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip

#Module 1: Repayment Module
while True:
    time.sleep(60)
    
    print (os.getcwd())
    #printing to make sure we are in code_folder, Repayments are stored 
    print (os.getcwd())# Changing code folder to Google Drive folder for Repayemnts
    
    os.chdir("..") #Going back a folder
    os.chdir("..") #Going back a folder
    os.chdir("..") #Going back a folder
    print (os.getcwd())
    
    
    os.chdir("Google Drive/Shared drives/Repayments") #Changing directory to Repayments
    
    
    #Because the repayments are stored date wise, selecting today's date and 
    #searching for a folder which has been named today's date'
    
    
    from datetime import date
    from datetime import timedelta
    today = date.today() #Getting today's date
    
    print("Today's date:", today) #Today's date is in timestamp format
    today = today.strftime("%d-%b-%Y")
    
    os.chdir(today)
    print (os.getcwd())
    # Checking to see if folder is correct
    # pd.read_excel(os.listdir()[0])
    
    
    #The repayments 
    #os.listdir() is listing the files in that folder. There is only one file every day thatis put manually by the tean on Google drive
    total_refunds_2_oct= pd.read_excel((os.listdir()[0]), sheet_name="Oct") #Reading sheet Oct and so on
    total_refunds_2_nov= pd.read_excel((os.listdir()[0]), sheet_name="Nov")
    total_refunds_2_dec= pd.read_excel((os.listdir()[0]), sheet_name="Dec")
    #combining all sheets into one consolidated dataframe column wise. 
    total_refunds_2 = pd.concat([total_refunds_2_oct,total_refunds_2_nov, total_refunds_2_dec])
    total_refunds_2.rename(columns={"Due date":"created at"},inplace=True)
    #Changing name of Due date to created at to make sure we don't have issues with Data Studio. DS takes only created at date now. 
    total_refunds_2.drop(["full_name"],1,inplace=True)
    total_refunds = total_refunds_2.copy()
    total_refunds["created at"] = total_refunds["created at"].astype(str)
    #Always changing date time to string (object in pandas ) to make sure we can filter dates effectively. 
    
    print (os.getcwd())
    os.chdir("/Users/arunabhmajumdar/Documents/all-india-dashboard-code/Code")
    
    #End of Repayment Module
    
    #Start of Quess Landing Page module metrics"
    

#Connects to Dynamo Db Items Quess landing Dropoff")

    print (os.getcwd())
    print ("ok")
    #     time.sleep(3)
    print ("Dynamo Update Landing Page")
    # time.sleep(60)
    print ("starting run")
    session = boto3.session.Session(profile_name="rain-india-production")
    client = session.client("dynamodb")
    dynamodb = boto3.resource("dynamodb")
    # table = dynamodb.Table("QuessLandingDropoff")
    response = client.scan(TableName = "QuessLandingDropoff")
    
    os.getcwd()
    os.chdir("..")
    os.chdir("AWS_Quess_Landing_Page")
    os.chdir("AWSDynamoDB/")
    os.chdir("json")
    os.chdir("data")
    all_files = []
    for x in os.listdir():
        print (x)
        f=gzip.open(x,'r')
        file_content=f.read()
        all_files.append(file_content)
    json_strings = []
    for x in all_files:
        my_json = x.decode('utf-8').replace("'",'"')
        json_strings.append(my_json)
    all_cleaned =[]
    for x in json_strings:
        all_cleaned.append(x.split("\n"))
    c = []
    c1 = []
    c2 = []
    c3 = []
    
    
    
    try:
        for x in all_cleaned[0]:
            c.append(ast.literal_eval(x))
    except:
        pass
    try:
        for x in all_cleaned[1]:
            c1.append(ast.literal_eval(x))
    except:
        pass
    try:
        for x in all_cleaned[2]:
            c2.append(ast.literal_eval(x))
    except:
        pass
    try:
        for x in all_cleaned[3]:
            c3.append(ast.literal_eval(x))
    except:
        pass
    c = c+c1+c2+c3
    employee_id = []
    updated_at = []
    created_at = []
    phone_number = []
    status = []
    for x in c:
    #     print (x)
        employee_id.append(x["Item"]["employee_id"])
        updated_at.append(x["Item"]["updated_at"])
        phone_number.append(x["Item"]["phone_number"])
        created_at.append(x["Item"]["created_at"])
        status.append(x["Item"]["status"])
    df = pd.DataFrame()
    df["employee_id"] = employee_id
    df["phone_number"] = phone_number
    df["status"] = status
    df["created_at"] = created_at
    df["updated_at"] = updated_at
    employee_id = []
    phone_number = []
    status = []
    created_at = []
    updated_at = []
    for x in range(df.shape[0]):
        employee_id.append(df["employee_id"].iloc[x]["S"])
        phone_number.append(df["phone_number"].iloc[x]["S"])
        status.append(df["status"].iloc[x]["S"])
        created_at.append(df["created_at"].iloc[x]["S"])
        updated_at.append(df["updated_at"].iloc[x]["S"])
    print (len(employee_id))
    print (len(phone_number))
    print (len(status))
    print (len(created_at))
    print (len(updated_at))
    
    df["employee_id"] = employee_id
    df["phone_number"] = phone_number
    df["status"] = status
    df["created_at"] = created_at
    df["updated_at"] = updated_at
    df = df.sort_values("created_at")
    os.chdir("..")
    os.chdir("..")
    os.chdir("..")
    os.chdir("..")
    os.chdir("Code/")
    
    # Google Shheet update code for Quess Funnel data backup on the CS-OPS Sheet
    scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
    creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
    client_2 = gspread.authorize(creds)
    employees_kyc_demographic= client_2.open("CS/OPS Dashboard").worksheet("New Quess Landing Page (updated)")
    employees_kyc_demographic.clear()
    existing = gd.get_as_dataframe(employees_kyc_demographic)
    existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
    updated = existing.append(df)
    gd.set_with_dataframe(employees_kyc_demographic, updated)
    
    print ("Starting Funnel")
    
    
    #The funnel data is coded into a dictionary called final which is then converted to a dataframe
    
    final = {}
    fields = ["Click on Rain Banner inside on Dash App",
    "Click on Product despcription Page",
    "User lands on Rain signup page",
    "Users landing on Password screen(password not yet created)",
    "User is created(Password has been set)",
    "Bureau Pull (Total attempts)"
             ]
    # source = ["Quess"]*2+["Rain"]*23
    for x in fields:
        final[x] = ""
    df["created_at"] = pd.to_datetime(df["created_at"]).dt.tz_convert(my_timezone)
    df["created_at"] = pd.to_datetime(df["created_at"]).dt.date.astype(str)
    df = df.set_index("created_at")
    df = df.loc["2021-09-01":]
    df["phone_number"] = df["phone_number"].astype(str)
    phone_number = []
    for x in df["phone_number"]:
        if len(x)==10:
            phone_number.append(x)
        else:
            phone_number.append(x[2:])
    df["phone_number"] = phone_number
    #     df["phone_number"] = df["phone_number"].astype(int)
    df["phone_number"] = df["phone_number"].astype(str)
    existing_2 = df.reset_index()
    
    
    
    connection = psycopg2.connect(user="rainadmin",
                                          password="Mudar123",
                                          host="localhost",
                                          port=55432,
                                          database="rain")
    cursor = connection.cursor()
    # Print PostgreSQL details
    print("PostgreSQL server information")
    print(connection.get_dsn_parameters(), "\n")
    
    cursor.execute("SELECT version();")
        # Fetch result
    record = cursor.fetchone()
    print("You are connected to - ", record, "\n")
    def dataframe_generator(query):
        cursor.execute(query)
        print('Read table in PostgreSQL')
        data = cursor.fetchall()
        cols = []
        for elt in cursor.description:
            cols.append(elt[0])
        df= pd.DataFrame(data = data, columns=cols)
        return df
    
    def clean(df):
        df["created_at"] = df["created_at"].dt.date.astype(str)
        df = df[df["created_at"]>"2021-08-31"]
        return df
    
    query = """select * from iam.users u ;"""
    iam = dataframe_generator(query)
    iam = clean(iam)
    iam.rename(columns={"id":"user_id"},inplace=True)
    phone_number = iam["phone_number"].astype(str).tolist()
    phone_numbers = []
    for x in phone_number:
        phone_numbers.append(re.sub("[^0-9]", "", x))
    phone_number_2 =[]
    for x in phone_numbers:
        if len(x)>10:
            phone_number_2.append(x[2:])
        else:
            phone_number_2.append(x)
    iam["phone_number"] = phone_number_2
    
    query = """select * from risk.user_risk_verifications urv; """
    cv = dataframe_generator(query)
    cv = clean(cv)
    
    cv.rename(columns={"score":"Approved"},inplace=True)
    
    os.chdir("..")
    os.chdir("AWS_Data/")
    rootdir = os.getcwd()
    files_dump =[]
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
    #             print(os.path.join(subdir, file))
            if file.endswith("json"):
                files_dump.append(os.path.join(subdir, file))
    ff = []
    for x in files_dump:
        fff = {}
        f = open(x)
        try:
            data = json.load(f)
        except:
            data = "Json Failure - Issue at our AWS end"
        fff["user_id"] = str(f).split("/")[-2]
        fff["data"] = data
        ff.append(fff)
    ffff = pd.DataFrame(ff)
    
    
    aws_approved = []
    for x in ffff["data"]:
        if x=="Yes":
            aws_approved.append(True)
        else:
            aws_approved.append(False)
    ffff["Aws Approved"] = aws_approved
    cv = pd.merge(cv,ffff, on = "user_id")
    
    
    final["User lands on Rain signup page"] = df["phone_number"].nunique()
    final["User is created(Password has been set)"] = iam.shape[0]
    final["Bureau Pull (Total attempts)"] = cv["user_id"].nunique()
    
    
    final["Overridden Bureau Approved"] = cv[cv["Approved"]==True]["user_id"].nunique()
    
    final["User reaches OTP Screen"] = iam.shape[0]
    final["User enters correct OTP"] = iam['status'].value_counts()[0]
    
    os.chdir("..")
    os.chdir("Code")
    play_store = pd.read_csv("All countries _ regions, India, Netherlands.csv")
    play_store = play_store.iloc[:,:2]
    from datetime import datetime
    dates = play_store["Date"].tolist()
    new_dates = []
    for x in dates:
        new_dates.append(datetime.strptime(x, '%b %d, %Y'))
    new_dates = [x.strftime("%Y-%m-%d") for x in new_dates]
    play_store["Date"] = new_dates
    play_store.columns = ["Date", "Installed Base"]
    play_store["Installed Base"] = play_store["Installed Base"].str.replace(",", "").astype(int)
    play_store = play_store[play_store["Date"]>"2021-08-30"]
    final["Total Installed Base"] = play_store["Installed Base"].iloc[-1]
    
    response = client.scan(TableName = "prod-auth-token")
    df_1= pd.DataFrame(response["Items"])
    
    access_token = []
    time_to_live = []
    id_ = []
    for x in range(df_1.shape[0]):
        access_token.append(df_1["AccessToken"].iloc[x]["S"])
        time_to_live.append(df_1["TimeToLive"].iloc[x]["N"])
        id_.append(df_1["ID"].iloc[x]["S"])
    
    df_1["AccessToken"] = access_token
    df_1["TimeToLive"] = time_to_live
    df_1["ID"] = id_
    
    ttl = df_1["TimeToLive"].astype(int).tolist()
    
    ttl = [datetime.fromtimestamp(x).strftime("%Y-%m-%d %H:%M:%S") for x in ttl]
    
    df_1["TimeToLive"]=ttl
    df_1["created_at"] = pd.to_datetime(df_1["TimeToLive"]).dt.date.astype(str)
    final["Total Signins"] = df_1.shape[0]
    
    query = """select * from kyc.documents kyc ;"""
    kyc = dataframe_generator(query)
    kyc = clean(kyc)
    final["KYC_Stage"] = kyc["user_id"].nunique()
    kyc = kyc[kyc["approved"]==True]
    
    uid = kyc["user_id"].unique().tolist()
    
    selfie_only= []
    selfie_and_pan = []
    all_3=[]
    for x in uid:
        d = kyc[kyc["user_id"]==x]["document_type"].value_counts().index.tolist()
        if "SILENTLIVENESS" in d:
            selfie_only.append(x)
        if "SILENTLIVENESS" in d and "PAN" in d:
            selfie_and_pan.append(x)
        if "SILENTLIVENESS" in d and "PAN" in d and "AADHAAR" in d:
            all_3.append(x)
    
    final["KYC Stage 1: Selfie approved"] = len(selfie_only)
    final["KYC Stage 2: Selfie and PAN approved"] = len(selfie_and_pan)
    final["KYC Stage 3: All 3 approved"] = len(all_3)
    
    final["KYC_XML"] = kyc[(kyc["document_type"]=="AADHAAR")&(kyc["side"]=="")]["user_id"].nunique()
    final["KYC_OCR"] = final["KYC Stage 3: All 3 approved"] - final["KYC_XML"]
    
    query = """select * from bnk.external_accounts ea ;"""
    bnk_external = dataframe_generator(query)
    bnk_external = clean(bnk_external)
    
    final["User completes penny drop"] =bnk_external[bnk_external["status"]=="ACTIVE"].shape[0]
    
    query = """select * from ems.loan_agreements la ;"""
    la = dataframe_generator(query)
    la = clean(la)
    la = la[1:]
    
    final["Loan Agreement Accepted"] = la.shape[0]
    
    query = """select * from bnk.transactions t  ;"""
    txns_2 = dataframe_generator(query)
    txns_2 = clean(txns_2)
    txns_2.rename(columns={"entity_id":"user_id"},inplace=True)
    txns_2.rename(columns={"amount":"Withdrawn Amount"}, inplace=True)
    # txns.rename(columns={"created_at":"disbursal(txn) date"},inplace=True)
    txns_2 = txns_2[txns_2["status"]=="COMPLETE"]
    txns_2 = txns_2[["id","user_id", "Withdrawn Amount","status", "created_at"]]
    txns_2["Withdrawn Amount"] = txns_2["Withdrawn Amount"]/100
    txns_2.rename(columns={"id":"tid"},inplace=True)
    
    
    query = """select * from ems.employees e ;"""
    ems_employees = dataframe_generator(query)
    ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
            'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
    ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
    ems_employees= clean(ems_employees)
    iam_users = iam[["user_id", "full_name", "email","phone_number", "metadata","document_number"]]
    iam_ems_employees = pd.merge(ems_employees,iam_users, on = "user_id")
    iam_ems_employees = iam_ems_employees.sort_values("created_at")
    gender=[]
    birth_date=[]
    for i in range(0,iam_ems_employees.shape[0]):
        gender.append(iam_ems_employees["metadata"].iloc[i]["gender"])
        birth_date.append(iam_ems_employees["metadata"].iloc[i]["birth_date"])
    
    iam_ems_employees["Gender"] = gender
    iam_ems_employees["birth_date"] = birth_date
    iam_ems_employees.drop(["metadata"],1,inplace=True)
    iam_ems_employees = iam_ems_employees[['user_id', 'employee_id', 'full_name', 'birth_date', 'Gender', 'employer_id', 'email', 'status', 'phone_number',
    'organization_id','document_number','created_at']]
    query = """select * from ems.compensations c ;"""
    compensations = dataframe_generator(query)
    compensations = clean(compensations)
    compensations = compensations[["employee_id", "monthly_salary"]]
    compensations["monthly_salary"] = compensations["monthly_salary"]/100
    rows_1_39 = pd.merge(iam_ems_employees,compensations, on = "employee_id")
    query = """select * from bnk.transactions t  ;"""
    txns = dataframe_generator(query)
    txns["second_creation_dummy"] = txns["created_at"]
    txns = clean(txns)
    txns.rename(columns={"entity_id":"user_id"},inplace=True)
    txns.rename(columns={"amount":"Withdrawn Amount"}, inplace=True)
    txns.rename(columns={"second_creation_dummy":"disbursal(txn) date"},inplace=True)
    # txns.rename(columns={"created_at":"disbursal(txn) date"},inplace=True)
    txns = txns[txns["status"]=="COMPLETE"]
    txns = txns[["id","user_id", "Withdrawn Amount","disbursal(txn) date"]]
    txns["Withdrawn Amount"] = txns["Withdrawn Amount"]/100
    txns.rename(columns={"id":"tid"},inplace=True)
    all_rows = pd.merge(rows_1_39, txns, on = "user_id")
    withdrawn_amounts=all_rows["Withdrawn Amount"].tolist()
    total_fees = []
    for x in withdrawn_amounts:
        if 50<x<301:
            total_fees.append(12)
        elif 300<x<501:
            total_fees.append(18)
        elif 500<x<1001:
            total_fees.append(35)
        elif 1000<x<1501:
            total_fees.append(60)
        elif 1500<x<2501:
            total_fees.append(100)
        elif 2500<x<5001:
            total_fees.append(200)
        elif 5000<x<10001:
            total_fees.append(275)
        elif 10000<x<25001:
            total_fees.append(700)
        elif 25000<x<50001:
            total_fees.append(800)
        elif 50000<x<100001:
            total_fees.append(1500)
        else:
            total_fees.append(0)
    all_rows["total_fees"] = total_fees
    all_rows["processing_fees"] = round(all_rows["total_fees"]/1.18,2)
    all_rows["GST_fees"] = all_rows["total_fees"] - all_rows["processing_fees"]
    all_rows.drop(["total_fees"],1,inplace = True)
    all_rows["Annual_income"] = (all_rows["monthly_salary"]*12).astype(float)
    all_rows["Annual_income"] = all_rows["Annual_income"].astype(str)
    all_rows["Total Amount"] = all_rows["Withdrawn Amount"]+all_rows["GST_fees"]+all_rows["processing_fees"]
    query = """select * from ems.work_period_balances wpb ;"""
    wpb = dataframe_generator(query)
    wpb = clean(wpb)
    wpb = wpb[["employee_id", "available_amount", "payment_date"]]
    wpb.rename(columns={"available_amount":"overall_limit", "payment_date":"next_payment_date"},inplace=True)
    wpb["next_payment_date"] = pd.to_datetime(wpb["next_payment_date"]).dt.date
    wpb["overall_limit"]  = wpb["overall_limit"]/100
    all_rows = pd.merge(all_rows, wpb, on = "employee_id")
    # all_rows["disbursal(txn) date"] = pd.to_datetime(all_rows["disbursal(txn) date"])
    all_rows = all_rows.sort_values("disbursal(txn) date", ascending=True)
    all_rows = all_rows.drop_duplicates(["tid"])
    all_rows = all_rows.sort_values("disbursal(txn) date", ascending=True)
    all_rows = all_rows.drop_duplicates(["tid"])
    all_rows["Sanctioned Loan Limit"] = all_rows["monthly_salary"]*0.4
    uid = all_rows["user_id"].unique().tolist()
    sum_of_withdrawals = []
    for x in uid:
        sum_of_withdrawals.append(all_rows[all_rows["user_id"]==x]["Withdrawn Amount"].sum())
    dddd = dict(zip(uid,sum_of_withdrawals))
    all_rows["disbursed amount"] = all_rows['user_id'].map(dddd)  
    all_rows["Undisbursed amount"] = all_rows["Sanctioned Loan Limit"] - all_rows["disbursed amount"]
    
    all_rows = all_rows.drop_duplicates(["tid"])
    all_rows["user_id"].nunique()
    all_rows["created_at"] = pd.to_datetime(all_rows["created_at"])
    
    
    all_rows = clean(all_rows)
    
    final["Total successful withdrawing users"] = all_rows["user_id"].nunique()
    
    final["Same Day Withdrawal"] = "For individual dates only"
    final["New Users Today"] = "For individual dates only"
    all_rows_grouped = all_rows.groupby("user_id").last().reset_index()
    # all_rows_grouped["created_at"] = pd.to_datetime(all_rows_grouped["created_at"]).dt.tz_localize(my_timezone)
    final["Total number of Withdrawals"] = all_rows.shape[0]
    final["Average Amount Disbursed"] = round(all_rows["Withdrawn Amount"].mean(),2)
    final["Median Amount Disbursed"] = np.median(all_rows["Withdrawn Amount"])
    final["Average Disbursal per user"] = round(all_rows.shape[0]/all_rows["user_id"].nunique(),2)
    final["Median Disbursal per user"] = np.median(all_rows["user_id"].value_counts().values)
    
    uid = all_rows["user_id"].unique().tolist()
    salaries = []
    for x in uid:
        salaries.append(all_rows[all_rows["user_id"]==x]["Sanctioned Loan Limit"].iloc[-1])
    d5 = dict(zip(uid,salaries))
    mapped = pd.DataFrame(d5.items())
    mapped["withdrawn"] = mapped[0].map(dddd)
    mapped = mapped.dropna()
    mapped["ut"] = mapped["withdrawn"]/mapped[1]
    mmm = round(mapped["ut"].mean()*100,2)
    
    #     final["Utilization of limit %"]  = mmm
    final["Repaid Amount Total"] = round(total_refunds["repayment"].sum(),2)
    
    def dates_generator_with_key(date):
        final_new = {}
        fields = ["Click on Rain Banner inside on Dash App",
        "Click on Product despcription Page",
        "User lands on Rain signup page",
        "Users landing on Password screen(password not yet created)",
        "User is created(Password has been set)",
        "Bureau Pull (Total attempts)"
                 ]
        # source = ["Quess"]*2+["Rain"]*23
        for x in fields:
            final_new[x] = ""
        existing_2_new = existing_2[existing_2["created_at"]==date]
        iam_new = iam[iam["created_at"]==date]
        final_new["User lands on Rain signup page"] = existing_2_new["phone_number"].nunique()
        final_new["User is created(Password has been set)"] = iam_new.shape[0]
        cv_new = cv[cv["created_at"]==date]
        final_new["Bureau Pull (Total attempts)"] = cv_new["user_id"].nunique()
    
    
        final_new["Overridden Bureau Approved"] = cv_new[cv_new["Approved"]==True]["user_id"].nunique()
        if iam_new.shape[0]>0:
            final_new["User reaches OTP Screen"] = iam_new.shape[0]
            final_new["User enters correct OTP"] = iam_new['status'].value_counts()[0]
        else:
            final_new["User reaches OTP Screen"] = ""
            final_new["User enters correct OTP"] = ""
    
        m = play_store[play_store["Date"]==date]["Installed Base"].values.tolist()[-1]
        n = play_store[play_store["Date"]<date]["Installed Base"].iloc[-1]
        final_new['Total Installed Base'] = m-n
        final_new["Total Signins"]  = df_1[df_1["created_at"]==date].shape[0]
        kyc_new = kyc[kyc["created_at"]==date]
        uid_new = kyc_new["user_id"].unique().tolist()
        final_new["KYC_Stage"] = len(uid_new)
    
        selfie_only_new= []
        selfie_and_pan_new = []
        all_3_new=[]
        for x in uid_new:
            d_new = kyc_new[kyc_new["user_id"]==x]["document_type"].value_counts().index.tolist()
            if "SILENTLIVENESS" in d_new:
                selfie_only_new.append(x)
            if "SILENTLIVENESS" in d_new and "PAN" in d_new:
                selfie_and_pan_new.append(x)
            if "SILENTLIVENESS" in d_new and "PAN" in d_new and "AADHAAR" in d_new:
                all_3_new.append(x)
    
        final_new["KYC Stage 1: Selfie approved"] = len(selfie_only_new)
        final_new["KYC Stage 2: Selfie and PAN approved"] = len(selfie_and_pan_new)
        final_new["KYC Stage 3: All 3 approved"] = len(all_3_new)
    
    
        final_new["KYC_XML"] = kyc_new[(kyc_new["document_type"]=="AADHAAR")&(kyc_new["side"]=="")]["user_id"].nunique()
        final_new["KYC_OCR"] = final_new["KYC Stage 3: All 3 approved"] - final_new["KYC_XML"]
    
    
        bnk_external_new = bnk_external[bnk_external["created_at"]==date]
        final_new["User completes penny drop"] =bnk_external_new[bnk_external_new["status"]=="ACTIVE"].shape[0]
        la_new= la[la["created_at"]==date]
        final_new["Loan Agreement Accepted"] = la_new.shape[0]
        txns_new= txns_2[txns_2["created_at"]==date]
        all_txns_uids = txns_new["user_id"].unique().tolist()
        all_uids_on_day = iam_new["user_id"].unique().tolist()
        sdw = len(list(set(all_uids_on_day).intersection(all_txns_uids)))
        all_rows["disbursal(txn) date"] = pd.to_datetime(all_rows["disbursal(txn) date"]).dt.date.astype(str)
        all_rows_new = all_rows[all_rows["disbursal(txn) date"]==date]
        try:
            final_new["Total successful withdrawing users"] = all_rows_new["user_id"].nunique()
            final_new["Same Day Withdrawal"] = sdw
            final_new["Total number of Withdrawals"] = all_rows_new.shape[0]
            final_new["Average Amount Disbursed"] = round(all_rows_new["Withdrawn Amount"].mean(),2)
    
            final_new["Median Amount Disbursed"] = np.median(all_rows_new["Withdrawn Amount"])
    
            final_new["Average Disbursal per user"] = round(all_rows_new.shape[0]/all_rows_new["user_id"].nunique(),2)
    
            final_new["Median Disbursal per user"] = np.median(all_rows_new["user_id"].value_counts().values)
    
            uid_new = all_rows_new["user_id"].unique().tolist()
            salaries_new = []
    
    
    
            sum_of_withdrawals_new = []
            for x in uid_new:
                sum_of_withdrawals_new.append(all_rows_new[all_rows_new["user_id"]==x]["Withdrawn Amount"].sum())
            dddd_new = dict(zip(uid_new,sum_of_withdrawals_new))
    
            for x in uid_new:
                salaries_new.append(all_rows_new[all_rows_new["user_id"]==x]["Sanctioned Loan Limit"].iloc[-1])
            d5_new = dict(zip(uid_new,salaries_new))
            mapped_new = pd.DataFrame(d5_new.items())
            mapped_new["withdrawn"] = mapped_new[0].map(dddd)
            mapped_new = mapped_new.dropna()
            mapped_new["ut"] = mapped_new["withdrawn"]/mapped_new[1]
            mmm_new = round(mapped_new["ut"].mean()*100,2)
    #             final_new["Utilization of limit %"]  = mmm_new
            final_new["New Users Today"] = all_rows_grouped[all_rows_grouped["created_at"]==date].shape[0]
    #         final_new["Repaid Amount Total (pg link)"] = refund_data[refund_data["created_at"]==date]["Amount"].sum()
            final_new["Repaid Amount Total"] = total_refunds[total_refunds["created at"]==date]["repayment"].sum()
        except:
            final_new["Total successful withdrawing users"] = all_rows_new["user_id"].nunique()
            final_new["Same Day Withdrawal"] = sdw
            final_new["Total number of Withdrawals"] = all_rows_new.shape[0]
            final_new["Average Amount Disbursed"] = np.nan
    
            final_new["Median Amount Disbursed"] = np.median(all_rows_new["Withdrawn Amount"])
    
            final_new["Average Disbursal per user"] = np.nan
    
            final_new["Median Disbursal per user"] = np.median(all_rows_new["user_id"].value_counts().values)
    #             final_new["Utilization of limit %"]  = np.nan
            final_new["New Users Today"] = 0
    #         final_new["Repaid Amount Total (pg link)"] = np.nan
            final_new["Repaid Amount Total"] = np.nan
        final_new_final = pd.DataFrame(final_new.items())
        final_new_final.columns = ["Funnel Stage", str(date)]
        return final_new_final
    
    
    def dates_generator(date):
            final_new = {}
            fields = ["Click on Rain Banner inside on Dash App",
            "Click on Product despcription Page",
            "User lands on Rain signup page",
            "Users landing on Password screen(password not yet created)",
            "User is created(Password has been set)",
            "Bureau Pull (Total attempts)"
                     ]
            # source = ["Quess"]*2+["Rain"]*23
            for x in fields:
                final_new[x] = ""
            existing_2_new = existing_2[existing_2["created_at"]==date]
            iam_new = iam[iam["created_at"]==date]
            final_new["User lands on Rain signup page"] = existing_2_new["phone_number"].nunique()
            final_new["User is created(Password has been set)"] = iam_new.shape[0]
            cv_new = cv[cv["created_at"]==date]
            final_new["Bureau Pull (Total attempts)"] = cv_new["user_id"].nunique()
    
    
            final_new["Overridden Bureau Approved"] = cv_new[cv_new["Approved"]==True]["user_id"].nunique()
            if iam_new.shape[0]>0:
                final_new["User reaches OTP Screen"] = iam_new.shape[0]
                final_new["User enters correct OTP"] = iam_new['status'].value_counts()[0]
            else:
                final_new["User reaches OTP Screen"] = ""
                final_new["User enters correct OTP"] = ""
    
            m = play_store[play_store["Date"]==date]["Installed Base"].values.tolist()[-1]
            n = play_store[play_store["Date"]<date]["Installed Base"].iloc[-1]
            final_new['Total Installed Base'] = m-n
            final_new["Total Signins"]  = df_1[df_1["created_at"]==date].shape[0]
            kyc_new = kyc[kyc["created_at"]==date]
            uid_new = kyc_new["user_id"].unique().tolist()
            final_new["KYC_Stage"] = len(uid_new)
    
            selfie_only_new= []
            selfie_and_pan_new = []
            all_3_new=[]
            for x in uid_new:
                d_new = kyc_new[kyc_new["user_id"]==x]["document_type"].value_counts().index.tolist()
                if "SILENTLIVENESS" in d_new:
                    selfie_only_new.append(x)
                if "SILENTLIVENESS" in d_new and "PAN" in d_new:
                    selfie_and_pan_new.append(x)
                if "SILENTLIVENESS" in d_new and "PAN" in d_new and "AADHAAR" in d_new:
                    all_3_new.append(x)
    
            final_new["KYC Stage 1: Selfie approved"] = len(selfie_only_new)
            final_new["KYC Stage 2: Selfie and PAN approved"] = len(selfie_and_pan_new)
            final_new["KYC Stage 3: All 3 approved"] = len(all_3_new)
    
    
            final_new["KYC_XML"] = kyc_new[(kyc_new["document_type"]=="AADHAAR")&(kyc_new["side"]=="")]["user_id"].nunique()
            final_new["KYC_OCR"] = final_new["KYC Stage 3: All 3 approved"] - final_new["KYC_XML"]
    
    
            bnk_external_new = bnk_external[bnk_external["created_at"]==date]
            final_new["User completes penny drop"] =bnk_external_new[bnk_external_new["status"]=="ACTIVE"].shape[0]
            la_new= la[la["created_at"]==date]
            final_new["Loan Agreement Accepted"] = la_new.shape[0]
            txns_new= txns_2[txns_2["created_at"]==date]
            all_txns_uids = txns_new["user_id"].unique().tolist()
            all_uids_on_day = iam_new["user_id"].unique().tolist()
            sdw = len(list(set(all_uids_on_day).intersection(all_txns_uids)))
            all_rows["disbursal(txn) date"] = pd.to_datetime(all_rows["disbursal(txn) date"]).dt.date.astype(str)
            all_rows_new = all_rows[all_rows["disbursal(txn) date"]==date]
            try:
                final_new["Total successful withdrawing users"] = all_rows_new["user_id"].nunique()
                final_new["Same Day Withdrawal"] = sdw
                final_new["Total number of Withdrawals"] = all_rows_new.shape[0]
                final_new["Average Amount Disbursed"] = round(all_rows_new["Withdrawn Amount"].mean(),2)
    
                final_new["Median Amount Disbursed"] = np.median(all_rows_new["Withdrawn Amount"])
    
                final_new["Average Disbursal per user"] = round(all_rows_new.shape[0]/all_rows_new["user_id"].nunique(),2)
    
                final_new["Median Disbursal per user"] = np.median(all_rows_new["user_id"].value_counts().values)
    
                uid_new = all_rows_new["user_id"].unique().tolist()
                salaries_new = []
    
    
    
                sum_of_withdrawals_new = []
                for x in uid_new:
                    sum_of_withdrawals_new.append(all_rows_new[all_rows_new["user_id"]==x]["Withdrawn Amount"].sum())
                dddd_new = dict(zip(uid_new,sum_of_withdrawals_new))
    
                for x in uid_new:
                    salaries_new.append(all_rows_new[all_rows_new["user_id"]==x]["Sanctioned Loan Limit"].iloc[-1])
                d5_new = dict(zip(uid_new,salaries_new))
                mapped_new = pd.DataFrame(d5_new.items())
                mapped_new["withdrawn"] = mapped_new[0].map(dddd)
                mapped_new = mapped_new.dropna()
                mapped_new["ut"] = mapped_new["withdrawn"]/mapped_new[1]
                mmm_new = round(mapped_new["ut"].mean()*100,2)
    #             final_new["Utilization of limit %"]  = mmm_new
                final_new["New Users Today"] = all_rows_grouped[all_rows_grouped["created_at"]==date].shape[0]
        #         final_new["Repaid Amount Total (pg link)"] = refund_data[refund_data["created_at"]==date]["Amount"].sum()
                final_new["Repaid Amount Total"] = total_refunds[total_refunds["created at"]==date]["repayment"].sum()
            except:
                final_new["Total successful withdrawing users"] = all_rows_new["user_id"].nunique()
                final_new["Same Day Withdrawal"] = sdw
                final_new["Total number of Withdrawals"] = all_rows_new.shape[0]
                final_new["Average Amount Disbursed"] = np.nan
    
                final_new["Median Amount Disbursed"] = np.median(all_rows_new["Withdrawn Amount"])
    
                final_new["Average Disbursal per user"] = np.nan
    
                final_new["Median Disbursal per user"] = np.median(all_rows_new["user_id"].value_counts().values)
    #             final_new["Utilization of limit %"]  = np.nan
                final_new["New Users Today"] = 0
        #         final_new["Repaid Amount Total (pg link)"] = np.nan
                final_new["Repaid Amount Total"] = np.nan
    
            final_new_final = pd.DataFrame(final_new.items())
            final_new_final.columns = ["Funnel Stage", str(date)]
            final_new_final.drop(["Funnel Stage"],1,inplace = True)
            return final_new_final
    
    
    f_test = dates_generator_with_key("2021-09-01")
    l = f_test["Funnel Stage"].tolist()
    sdate = date(2021,9,1)
    edate = pd.to_datetime("today").strftime('%Y-%m-%d')
    dates = pd.date_range(sdate,edate,freq='d').strftime('%Y-%m-%d').tolist()
    
    
    
    
    all_others = []
    for x in dates:
        all_others.append(dates_generator(x))
    test = pd.concat(all_others,1)
    test["Funnel Stage"] = l
    final = pd.DataFrame(final.items())
    final.columns = ["Funnel Stage", "Total till date"]
    test_2 = pd.merge(final, test, on= "Funnel Stage")
    
    test_2.to_csv("Funnel.csv")
    df = pd.read_csv('Funnel.csv')
    df.drop(["Unnamed: 0"],1,inplace=True)
    df = df.fillna("")
    funnel = client_2.open("CS/OPS Dashboard").worksheet("Quess_Funnel Data Backup")
    funnel.clear()
    existing = gd.get_as_dataframe(funnel)
    existing = pd.DataFrame(funnel.get_all_records())
    updated = existing.append(df)
    gd.set_with_dataframe(funnel,updated)
    print ("Successful Run - Funnel updated")
    
