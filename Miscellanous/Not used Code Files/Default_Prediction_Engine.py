#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 11:01:21 2022

@author: arunabhmajumdar
"""

print ("Importing all packages and Google BQ credentials files")
import warnings
warnings.filterwarnings("ignore")
import pandas as pd,os
import base64
import boto3
import time
import pandas as pd, os
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import gspread_dataframe as gd
import psycopg2
import df2gspread as d2g
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import re
import calendar
import os
import json
import time
import numpy as np
import pytz
my_timezone = pytz.timezone('Asia/Calcutta')
import ast
import gzip
from datetime import timedelta
from currency_converter import CurrencyConverter
from google.oauth2 import service_account
from google.cloud import bigquery
import pandas_gbq
import os, pandas as pd
os.getcwd()
KEY_PATH = "data-warehouse-india-84f5f8a775d1.json"
CREDS = service_account.Credentials.from_service_account_file(KEY_PATH)
bq_client = bigquery.Client(credentials=CREDS, project="data-warehouse-india")
import warnings
warnings.filterwarnings("ignore")


print ("Connecting to Dynamo DB using boto3")
start = time.time()
print ("starting run")
session = boto3.session.Session(profile_name="rain-india-production")
client = session.client("dynamodb")
dynamodb = boto3.resource("dynamodb")
print ("Connecting to Postgres using psycopg2")
connection = psycopg2.connect(user="rainadmin",
                                      password="Mudar123",
                                      host="localhost",
                                      port=55432,
                                      database="rain")
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")
# cursor.itersize = 10000
cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")

print ("Function to download Postgres data and concert that to dataframe")
def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

print ("Function to clean dataframe to include only data post Sept 1, 2021")
def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df
print (time.time() - start)


print ("Function to clean dataframe to upload to BQ, BQ doesn't take spaces, dashes etc;")
def bq_cleaner(df):
    new_cols = []
    l = df.columns.tolist()
    for x in l:
        x = x.replace("(","_")
        x = x.replace(" ","_")
        x = x.replace(")","_")
        x = x.replace("%","_")
        x = x.replace("-","_")
        
        new_cols.append(x)
    df.columns = new_cols
    df = df.astype(str)
    return df





print ("Connecting to Repayment_Analysis Google Sheet")
start = time.time()
scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
            "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client_2 = gspread.authorize(creds)
employees_kyc_demographic= client_2.open("Repayment_analysis").worksheet("Till December data")
existing = gd.get_as_dataframe(employees_kyc_demographic)
existing = pd.DataFrame(employees_kyc_demographic.get_all_records())
repayment = existing.copy()

print ("Uploading Repayment Analysis Data to BQ")
repayment_bq = bq_cleaner(repayment)
pandas_gbq.to_gbq(repayment_bq, destination_table="Processed_data.Repayment_Analysis_Data", project_id="data-warehouse-india", if_exists="replace")







