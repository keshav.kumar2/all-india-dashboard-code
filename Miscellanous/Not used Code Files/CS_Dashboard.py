import os
import psycopg2
import gspread
# import df2gspread as d2g
import pandas as pd
import warnings
import time
warnings.filterwarnings("ignore")
from oauth2client.service_account import ServiceAccountCredentials
import gspread_dataframe as gd
import re



print ("Starting Run")
connection = psycopg2.connect(user="rainadmin",
                                  password="Mudar123",
                                  host="localhost",
                                  port=55432,
                                  database="rain")

    # Create a cursor to perform database operations
cursor = connection.cursor()
# Print PostgreSQL details
print("PostgreSQL server information")
print(connection.get_dsn_parameters(), "\n")

cursor.execute("SELECT version();")
    # Fetch result
record = cursor.fetchone()
print("You are connected to - ", record, "\n")


def dataframe_generator(query):
    cursor.execute(query)
    print('Read table in PostgreSQL')
    data = cursor.fetchall()
    cols = []
    for elt in cursor.description:
        cols.append(elt[0])
    df= pd.DataFrame(data = data, columns=cols)
    return df

def clean(df):
    df["created_at"] = df["created_at"].dt.date.astype(str)
    df = df[df["created_at"]>"2021-08-31"]
    return df

query = """select * from ems.employees e ;"""
ems_employees = dataframe_generator(query)
ems_employees = ems_employees[['id', 'user_id', 'employer_id', 'status', 'first_name', 'last_name',
       'created_at', 'updated_at', 'group_id', 'hr_employee_code', 'organization_id']]
ems_employees = ems_employees.rename(columns={'id': 'employee_id'})
print (ems_employees.shape)
ems_employees = clean(ems_employees)



query = """select * from iam.users u ;"""
iam_users = dataframe_generator(query)
iam_users = iam_users.rename(columns={'id': 'user_id'})
iam_users = iam_users[["user_id", "full_name", "email","phone_number", "metadata","document_number", "created_at"]]
iam_users = clean(iam_users)
phone_number = iam_users["phone_number"].astype(str).tolist()
phone_numbers = []
for x in phone_number:
    phone_numbers.append(re.sub("[^0-9]", "", x))
phone_number_2 =[]
for x in phone_numbers:
    if len(x)>10:
        phone_number_2.append(x[2:])
    else:
        phone_number_2.append(x)
iam_users["phone_number"] = phone_number_2
iam_users.drop(["created_at"],1,inplace = True)
iam_ems_employees = pd.merge(ems_employees,iam_users, on = "user_id")
gender=[]
birth_date=[]
for i in range(0,iam_ems_employees.shape[0]):
    gender.append(iam_ems_employees["metadata"].iloc[i]["gender"])
    birth_date.append(iam_ems_employees["metadata"].iloc[i]["birth_date"])
    
    
iam_ems_employees["Gender"] = gender
iam_ems_employees["Birthdate"] = birth_date


gender=[]
birth_date=[]
for i in range(0,iam_ems_employees.shape[0]):
    gender.append(iam_ems_employees["metadata"].iloc[i]["gender"])
    birth_date.append(iam_ems_employees["metadata"].iloc[i]["birth_date"])
    
    
iam_ems_employees["Gender"] = gender
iam_ems_employees["Birthdate"] = birth_date


print (iam_ems_employees.shape)

iam_ems_employees.drop(["metadata"],1,inplace=True)



query = """select * from ems.compensations c ;"""
compensations = dataframe_generator(query)
compensations = clean(compensations)
compensations = compensations[["employee_id", "monthly_salary"]]
compensations["monthly_salary"] = compensations["monthly_salary"]/100
rows_1_39 = pd.merge(iam_ems_employees,compensations, on = "employee_id")
rows_1_39 = rows_1_39.sort_values("created_at")
os.chdir("..")
os.chdir("Outputs")
rows_1_39.to_csv("test.csv")
os.chdir("..")
os.chdir("Code")

scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", 
        "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("cs-and-ops-dashboard-8febbecf58a8.json", scope)
client = gspread.authorize(creds)

os.chdir("..")
os.chdir("Outputs")
employees_kyc_demographic_non_unique= client.open("CS/OPS Dashboard").worksheet("All signups")
employees_kyc_demographic_non_unique.clear()
existing = gd.get_as_dataframe(employees_kyc_demographic_non_unique)
existing = pd.DataFrame(employees_kyc_demographic_non_unique.get_all_records())
df = pd.read_csv("test.csv")
updated = existing.append(df)
gd.set_with_dataframe(employees_kyc_demographic_non_unique, updated)

    
